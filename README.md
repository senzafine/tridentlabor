# [Spiral](http://senzafine.net/spiral/)

__Spiral__ is modern WordPress stack forked from Bedrock by Roots using the best development and testing tools..

## Features

*   Better folder structure
*   Dependency management with [Composer](http://getcomposer.org)
*   Easy WordPress configuration with environment specific files
*   Environment variables with [Dotenv](https://github.com/vlucas/phpdotenv)
*   Autoloader for mu-plugins (use regular plugins as mu-plugins)
*   Enhanced security (separated web root and secure passwords with [wp-password-bcrypt](https://github.com/roots/wp-password-bcrypt))
*   Database sync between local and remote server
*   Uploads sync between local and remote server
*   One-command deploys with Capistrano

## Requirements

*   PHP >= 7.0
*   Composer
*   Node.js
*   NPM
*   Grunt
*   Ruby

## Installation

1.  Create a new project in a new folder for your project:
    ```
    $ git clone git@gitlab.com:senzafine/coil.git [project name]
    ```

2.  Go into the project folder and install the Composer packages:
    ```
    $ composer install
    ```

3.  Copy `.env.example` to `.env`
    ```
    $ cp .env.example .env
    ```

4.  Update environment variables:
    *   `DB_NAME`: Database name
    *   `DB_USER`: Database user
    *   `DB_PASSWORD`: Database password
    *   `DB_HOST`: Database host
    *   `DB_PREFIX`: Tables prefix
    *   `WP_ENV`: Set to environment (`development`, `staging`, `production`)
    *   `WP_HOME`: Full URL to WordPress home (http://example.com)
    *   `WP_SITEURL`: Full URL to WordPress including subdirectory (http://example.com/wp)
    *   `AUTH_KEY`, `SECURE_AUTH_KEY`, `LOGGED_IN_KEY`, `NONCE_KEY`, `AUTH_SALT`, `SECURE_AUTH_SALT`, `LOGGED_IN_SALT`, `NONCE_SALT`

    To automatically generate the security keys with WP-CLI install [wp-cli-dotenv-command](https://github.com/aaemnnosttv/wp-cli-dotenv-command):
    ```
    $ wp package install aaemnnosttv/wp-cli-dotenv-command
    ```

    And generate the keys with this command
    ```
    $ wp dotenv salts regenerate
    ```

    Alternatively you can use the [WordPress Salt Generator](https://api.wordpress.org/secret-key/1.1/salt/).

5.  Add theme(s) in `web/app/themes` as you would for a normal WordPress site.

6.  Set your site `vhost` document root to `[website path]/web/` (`[website path]/current/web/` if using deploys).

7.  Access WP admin at `http://example.com/wp/wp-admin`

## Deployment

1.  Install Bundler:
    ```
    $ gem install bundler
    ```

2.  Install the required gems:
    ```
    $ bundle install
    ```

3.  Before the first deployment create the necessary folders and symlinks:
    ```
    $ bundle exec cap [stage] deploy:check
    ```

4.  Copy and edit `.env` to your server in the `shared` directory and edit it for the remote environment

5.  Copy `.htaccess` to the remote `shared/web` directory

6.  Afterwards run the following command to deploy:
    ```
    $ bundle exec cap [stage] deploy
    ```

## Plugins

Install plugins with Composer and WPackagist with this command:
```
$ composer require [vendor]/[package]:[version]
```

## Spiral Theme

Spiral includes a default theme which you can build upon. Check the `README.md` file on the theme directory at `web/app/themes/spiral`
