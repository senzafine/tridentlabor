### 2.1.6: 2017-04-10
* Removed unused Composer dependencies

### 2.1.5: 2017-03-29
* Changed mysql.json to server.json to include rsync settings.
* Fixed deploy common deploy errors by updating bundler dependencies.

### 1.5.0: 2016-10-13
* Integrated with Spiral starter theme into one stack.

### 1.1.0: 2016-10-12
* Updated .env.example for better default settings.
* Updated application and development config files to better separate development and production configuration.
* Added setup mu plugin to set default options for the WordPress installation.

### 1.0.1: 2016-10-05
* Rewritten README and checked dependencies. First official commit.

### 1.0.0: 2016-09-07
* Beta release.
