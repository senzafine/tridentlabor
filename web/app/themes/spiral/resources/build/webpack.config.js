const path                = require('path');
const webpack             = require('webpack');
const merge               = require('webpack-merge');
const tasks               = require('./webpack.config.tasks.js');
const chunkManifestPlugin = require("chunk-manifest-webpack-plugin");
const webpackChunkHash    = require("webpack-chunk-hash");
const svgStore            = require('webpack-svgstore-plugin');
const packages            = require(path.resolve('package.json'));
const sources             = path.resolve('resources');

const paths = {
  scripts  : path.join(sources, 'scripts'),
  styles   : path.join(sources, 'styles'),
  fonts    : path.join(sources, 'fonts'),
  images   : path.join(sources, 'images'),
  node     : path.resolve('node_modules'),
  build    : path.resolve('assets'),
  public   : '/app/themes/spiral/assets/'
};

const sharedConfig = merge([
  {
    entry: {
      main      : [
        paths.scripts + '/main',
        paths.styles + '/main.styl'
      ],
      admin     : [
        paths.scripts + '/admin',
        paths.styles + '/admin.styl'
      ],
      editor    : [
        paths.scripts + '/editor',
        paths.styles + '/editor.styl'
      ],
      login     : [
        paths.scripts + '/login',
        paths.styles + '/login.styl'
      ],
      customizer: paths.scripts + '/customizer',
      vendor    : [
        paths.scripts + '/vendor',
        paths.styles + '/vendor.styl'
      ]
    },

    plugins: [
      new webpack.optimize.CommonsChunkPlugin({
        name     : ['vendor','manifest'],
        minChunks: Infinity
      }),

      new svgStore({
        svgoOptions: {
          plugins: [
            {
              removeTitle: true
            }
          ]
        },
        prefix: ''
      }),

      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery'
      })
    ],

    resolve: {
      extensions: ['.js','.css','.styl','.php','.svg', '.eot', '.png'],
      modules   : [path.join(__dirname, 'src'), 'node_modules']
    }
  },

  tasks.clean({
    path: paths.build
  }),

  tasks.lintScripts({
    path: paths.scripts
  }),

  tasks.setupScripts({
    path: paths.scripts
  }),

  tasks.lintStyles({
    path: paths.styles
  }),

  tasks.setupFonts({
    paths: paths
  }),

  tasks.setupImages({
    paths: paths
  })
]);

const developmentConfig = merge([
  tasks.sourceMaps({
    type: 'cheap-module-eval-source-map'
  }),

  tasks.setupOutput({
    paths: paths,
    options: {
      filename: 'scripts/[name].js',
      sourceMapFilename: 'sourcemaps/[name].map'
    }
  }),

  tasks.testStyles({
    path: paths.styles
  }),

  tasks.setFreeVariable(
    'process.env.NODE_ENV',
    'development'
  ),

  tasks.browserSyncServer({
    paths: paths,
    options : {
      host : '192.168.137.1',
      port : 3000,
      proxy  : 'http://tridentlabor.dev',
      files: [
        'app/**/*.php',
        'resources/*.php',
        'resources/assets/scripts/**/*.*',
        'resources/assets/images/**/*.*',
        'resources/views/**/*.php'
      ]
    }
  })
]);

const productionConfig = merge([
  tasks.sourceMaps({
    type: 'source-map'
  }),

  tasks.setupOutput({
    paths: paths,
    options: {
      filename: 'scripts/[name].[chunkhash].js',
      sourceMapFilename: 'sourcemaps/[name].[chunkhash].map'
    }
  }),

  tasks.setupStyles({
    path: paths.styles
  }),

  tasks.minify(),

  tasks.assets({
    path: paths.build
  }),

  tasks.setFreeVariable(
    'process.env.NODE_ENV',
    'production'
  )
]);


module.exports = (env) => {
  if (env === 'production') {
    return merge(sharedConfig, productionConfig);
  }

  return merge(sharedConfig, developmentConfig);
};
