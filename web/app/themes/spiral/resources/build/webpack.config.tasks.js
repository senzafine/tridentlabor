/** Global Plugins */
const path                    = require('path');
const webpack                 = require('webpack');

/** Development Plugins */
const browserSyncPlugin       = require('browser-sync-webpack-plugin');

/** Build Plugins */
const cleanWebpackPlugin      = require('clean-webpack-plugin');
const extractTextPlugin       = require('extract-text-webpack-plugin');
const writeFilePlugin         = require('write-file-webpack-plugin');
const copyWebpackPlugin       = require('copy-webpack-plugin');
const manifestPlugin            = require('webpack-assets-manifest');

/** Javascript Plugins */
const uglifyJSPlugin          = require('uglifyjs-webpack-plugin');

/** CSS Plugins */
const autoprefixer            = require('autoprefixer');
const optimizeCssassetsPlugin = require('optimize-css-assets-webpack-plugin');
const cssnano                 = require('cssnano');

exports.clean = ({path}) => ({
  plugins: [
    new cleanWebpackPlugin([path], {
      exclude: ['.gitkeep'],
      root: process.cwd(),
      verbose: false
    })
  ]
});

exports.browserSyncServer = ({paths, options} = {}) => ({
  devServer: {
    hot        : true,
    hotOnly    : true,
    inline     : true,
    contentBase: paths.build + '/',
    stats      : 'errors-only',
    publicPath : paths.public,
    overlay: {
      errors  : true,
      warnings: true
    }
  },

  plugins: [
    new browserSyncPlugin(
      {
        host : options.host,
        port : options.port,
        proxy: options.proxy,
        files: options.files
      },
      {
        reload: false
      }
    ),

    new webpack.WatchIgnorePlugin([
      paths.node
    ]),

    new webpack.HotModuleReplacementPlugin({
      multiStep: false
    }),

    new webpack.NoEmitOnErrorsPlugin(),

    new webpack.optimize.OccurrenceOrderPlugin(),

    new webpack.NamedModulesPlugin(),

    new writeFilePlugin({
      force: true,
      log: false
    })
  ]
});

exports.setupOutput = ({paths, options}) => ({
  output: {
    path             : paths.build,
    filename         : options.filename,
    publicPath       : paths.public,
    sourceMapFilename: options.sourceMapFilename
  }
});

exports.lintScripts = ({path}) => ({
  module: {
    rules: [
      {
        test   : /\.js$/,
        include: path,
        enforce: 'pre',
        loader : 'eslint-loader',
        options: {
          emitWarning: true,
          fix        : true
        }
      }
    ]
  }
});

exports.setupScripts = ({path}) => ({
  module: {
    rules: [
      {
        test   : /\.js$/,
        include: path,
        loader : 'babel-loader',
        options: {
          presets: ['env']
        }
      }
    ]
  }
});

exports.lintStyles = ({path}) => ({
  module: {
    rules: [
      {
        test: /\.css$/,
        include: path,
        enforce: 'pre',
        loader : 'postcss-loader',
        options: {
          plugins: () => ([
            require('stylelint')
          ])
        }
      },

      {
        test   : /\.styl$/,
        include: path,
        loader : 'stylint-loader',
        enforce: 'pre'
      }
    ]
  }
});

exports.testStyles = ({path}) => ({
  module: {
    rules: [
      {
        test: /\.css$/,
        use : [
          'style-loader',
          'css-loader',
          'resolve-url-loader'
        ]
      },

      {
        test   : /\.styl$/,
        include: path,
        use    : [
          'style-loader',
          'css-loader',
          'resolve-url-loader',
          'stylus-loader'
        ]
      }
    ]
  }
});

exports.setupStyles = ({stylesPath}) => ({
  module: {
    rules: [
      {
        test : /\.css$/,
        use  : extractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader'
            },
            {
              loader: 'postcss-loader',
              options: {
                plugins: () => ([
                  require('autoprefixer')
                ])
              }
            },
            {
              loader: 'resolve-url-loader'
            }
          ]
        })
      },

      {
        test   : /\.styl$/,
        include: stylesPath,
        use    : extractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader'
            },
            {
              loader: 'postcss-loader',
              options: {
                plugins: () => ([
                  require('autoprefixer')
                ])
              }
            },
            {
              loader: 'resolve-url-loader'
            },
            {
              loader: 'stylus-loader'
            }
          ]
        })
      }
    ]
  },

  plugins: [
    new extractTextPlugin({
      filename: 'styles/[name].[chunkhash].css'
    })
  ]
});

exports.setupImages = ({paths}) => ({
  module: {
    rules: [
      {
        test   : /\.(png|jpe?g|gif)$/,
        exclude: paths.images +'/sprites',
        use    : [
          {
            loader: 'file-loader',
            options: {
              name: 'images/[name].[ext]'
            }
          },

          {
            loader: 'image-webpack-loader',
            options : {
              mozjpeg: {
                progressive: true
              },
              gifsicle: {
                interlaced: false
              },
              optipng: {
                optimizationLevel: 7
              },
              pngquant: {
                quality: '75-90',
                speed  : 3
              }
            }
          }
        ]
      },

      {
        test   : /\.svg$/,
        include: paths.images +'/images',
        exclude: paths.images +'/sprites',
        use    : [
          {
            loader: 'file-loader',
            options: {
              name: 'images/[name].[ext]'
            }
          },

          {
            loader: 'svgo-loader',
            options: {
              plugins: [
                {
                  cleanupAttrs : true,
                  removeTitle  : true,
                  convertColors: true
                }
              ]
            }
          }
        ]
      }
    ]
  },

  plugins: [
    new copyWebpackPlugin([
      {
        from: paths.images,
        to  : paths.build + '/images'
      }
    ], {
      ignore: [
        'icons/*.*',
        'sprites/*.*',
        'slides/*.*',
        '*.ai',
        '.gitkeep'
      ]
    })
  ]
});

exports.setupFonts = ({paths}) => ({
  module: {
    rules: [
      {
        test: /\.(svg|ttf|eot|woff|woff2?)(\?[a-z0-9 =&.]+)?$/,
        loader : 'file-loader',
        options: {
          limit   : 50000,
          mimetype: 'application/font-woff',
          name    : 'fonts/[name].[ext]'
        }
      }
    ]
  }
});

exports.minify = () => ({
  plugins: [
    new uglifyJSPlugin({
      comments: false
    }),

    new optimizeCssassetsPlugin({
      cssProcessor       : cssnano,
      canPrint           : false,
      cssProcessorOptions: {
        discardComments: {
          removeAll: true
        },
        safe: true
      }
    })
  ]
});

exports.sourceMaps = ({type}) => ({
  devtool: type
});

exports.assets = () => ({
  plugins: [
    new manifestPlugin({
      output: 'assets.json',
      writeToDisk: true,
      replacer: function(key, value) {
        if (typeof value === 'string') {
          return value;
        }

        const manifest = value;

        Object.keys(manifest).forEach((src) => {
          const sourcePath = path.basename(path.dirname(src));
          const targetPath = path.basename(path.dirname(manifest[src]));

          if (sourcePath === targetPath) {
            return;
          }

          manifest[`${targetPath}/${src}`] = manifest[src];
          delete manifest[src];
        });

        return manifest;
      }
    })
  ]
});

exports.setFreeVariable = (key, value) => {
  const env = {};
  env[key] = JSON.stringify(value);

  return {
    plugins: [
      new webpack.DefinePlugin(env)
    ]
  }
}
