/**
 * Vendor Scripts
 */

/* Import installed node modules */
import 'jquery';
import 'swiper';
import 'lightgallery';
import 'jquery-touchswipe';

/* Import Lightgallery plugins */
require('lg-zoom');
require('lg-fullscreen');
