export default {
  init() {
    /** Define Swiper settings */
    var frontSwiper = new Swiper ('.swiper-container', {
      autoplay: 2500,
      effect: 'fade',
      slidesPerView: 1,
      centeredSlides: true,
      grabCursor: true,
      pagination: '.swiper-pagination',
      paginationClickable: true,
      nextButton: '.swiper-button-next',
      prevButton: '.swiper-button-prev',
      keyboardControl: true,
      mousewheelControl: false,
      loop: true,
      autoplayDisableOnInteraction: false
    });
  },
  finalize() {
  }
};
