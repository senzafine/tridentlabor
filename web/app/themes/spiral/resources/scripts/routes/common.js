export default {
  init() {
  },
  finalize() {
    /** Define Lightgallery settings */
    $(document).ready(function() {
      $('.entry-body').lightGallery({
        mode: 'lg-fade',
        download: false,
        selector: 'figure a'
      });
    });

    /** Add smooth scrolling to links in the same page */
    $('a[href*="#"]:not([href="#"])').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');

        if (target.length) {
          $('html, body').animate({
            scrollTop: target.offset().top
          }, 1000);
          return false;
        }
      }
    });

    /** Define window scrolling functions */
    $(window).scroll(function() {
      var scroll = getCurrentScroll();

      if (scroll >= $('.header-main').height()) {
        $('.button-top').addClass('scrolling');
      }

      if (scroll + $(window).height() > $(document).height() - $('.footer-main').height() || $(window).scrollTop() < $('.header-main').height()) {
        $('.button-top').removeClass('scrolling');
      }

      if (scroll + $(window).height() > $(document).height() - $('.footer-main').height()) {
        $('.toggle-sidebar').addClass('bottom-stop');
      } else {
        $('.toggle-sidebar').removeClass('bottom-stop');
      }
    });

    function getCurrentScroll() {
      return window.pageYOffset || document.documentElement.scrollTop;
    }

    /** Define jQuery swipe settings */
    $(function() {
      $('.sidebar-display .content').swipe({
        swipeLeft:function(event, direction, distance, duration, fingerCount, fingerData) {
          $('.content').addClass('sidebar-swipe');
          $('#toggle-sidebar').prop('checked', true);
        },

        swipeRight:function(event, direction, distance, duration, fingerCount, fingerData) {
          $('.content').removeClass('sidebar-swipe');
          $('#toggle-sidebar').prop('checked', false);
        }
      });
    });
  }
};
