/** import external dependencies */
import 'jquery';

/** import local dependencies */
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';

/**
 * Populate Router instance with DOM routes
 * @type {Router} routes - An instance of our router
 */
const routes = new Router({
  /** All pages */
  common,
  /** Home page */
  home
});

/** Load Events */
jQuery(document).ready(() => routes.loadEvents());
// document.addEventListener('DOMContentLoaded', () => new Router(routes).loadEvents());

/** Define and import the sprites directory */
const __svg__ = {
  path: '../images/sprites/*.svg',
  name: 'images/sprite.svg'
};

require('webpack-svgstore-plugin/src/helpers/svgxhr')(__svg__);
