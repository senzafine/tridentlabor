/**
 * WordPress Dashboard Scripts
 */

/* Define the address of the WordPress AJAX function file */
var ajax_url = '/wp/wp-admin/admin-ajax.php';

/* Setup functions */
(function($) {
  $('#soundtrack-details, #soundtrack-tracklist').on('focus', '.input-names', function() {
    $(this).suggest(ajax_url + '?action=spiral_get_names');
  });

  $('.names').on('click', '.name-add', function() {
    var $name = $(this).next().clone();

    $name.find('input').val('');
    $name.appendTo($(this).parent());
  });

  $('.names').on('click', '.name-remove', function() {
    $(this).parent().remove();
  });

  $('.track-add').click(function() {
    var current_track = $('.soundtrack-track:last-child .soundtrack-number input').val();
    current_track++;

    var $clone = $('.soundtrack-tracklist .soundtrack-track:last-child').clone();

    $clone.find('.soundtrack-number input').val(current_track);
    $clone.find('.soundtrack-name input').val('');
    $clone.find('.soundtrack-minutes input').val('');
    $clone.find('.soundtrack-seconds input').val('');

    $clone.appendTo($('.soundtrack-tracklist tbody'));
  });

  $('.track-view').click(function() {
    var data = $_POST['track'];
    alert(data);
  });

  $('.soundtrack-tracklist').on('click', '.track-remove', function() {
    $(this).parent().parent().remove();
  });
})(jQuery);
