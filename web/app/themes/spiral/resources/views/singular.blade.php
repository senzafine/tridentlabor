@extends('layouts.app')

@section('content')
  @while(have_posts())
    @php(the_post())

    <article @php(post_class('entry-single'))>
      <header class="entry-header">
        <h1>{{ get_the_title() }}</h1>
        @include('partials.meta-'.get_post_type())
      </header>

      <div class="entry-body">
        @include('partials.content-'.get_post_type())
      </div>

      @if (has_tag() || is_page() || spiral_is_paginated())
        <footer class="entry-footer">
          @if (has_tag())
            <div class="entry-tags">
              {!! get_the_tag_list('', '', '') !!}
            </div>
          @endif

          @if (is_paged() || $paged || spiral_is_paginated())
            {!! spiral_post_pagination() !!}
          @endif
        </footer>
      @endif

      @if (comments_open() || pings_open() || have_comments())
        <div id="discussion">
          {!! comments_template() !!}
        </div>
      @endif
    </article>
  @endwhile
@endsection
