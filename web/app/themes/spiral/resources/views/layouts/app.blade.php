<!doctype html>
<html @php(language_attributes()) xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml">
  @include('partials.head')
  <body id="top" @php(body_class())>
    <!--[if IE]>
      <div class="alert alert-warning">
        {{ __('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'spiral') }}
      </div>
    <![endif]-->

    @php(do_action('get_header'))
    @include('partials.header')

    <div role="document" class="container">
      @if (is_home() && ! is_paged())
        @include('partials.slider')
      @endif

      <input type="checkbox" id="toggle-sidebar" class="toggle-checkbox">
      <label for="toggle-sidebar" class="toggle-label toggle-sidebar" aria-label="{{ __('Sidebar', 'spiral') }}">
        <svg aria-hidden="true" class="icon icon-sidebar">
          <use xlink:href="{{ get_stylesheet_directory_uri() }}/assets/images/sprite.svg#sidebar_open">
        </svg>
      </label>

      <div class="wrapper content">
        <main class="entry">
          @yield('content')
        </main>

        @if (App\display_sidebar())
          @include('partials.sidebar')
        @endif
      </div>
    </div>

    <a href="#top" aria-label="{{ __('Up', 'spiral') }}" class="button-top">
      <svg aria-hidden="true" class="icon">
        <use xlink:href="{{ get_stylesheet_directory_uri() }}/assets/images/sprite.svg#up">
      </svg>
    </a>

    @php(do_action('get_footer'))
    @include('partials.footer')
    @php(wp_footer())
  </body>
</html>
