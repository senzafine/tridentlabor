@extends('layouts.app')

@section('content')
  @include('partials.content-header')

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __("These aren't the droids you're looking for.", 'spiral') }}
    </div>
    @include('partials.searchform')
  @endif
@endsection
