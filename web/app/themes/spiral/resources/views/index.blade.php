@extends('layouts.app')

@section('content')
  @include('partials.content-header')
  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __("These aren't the droids you're looking for.", 'spiral') }}
    </div>
    @include('partials.searchform')
  @else
    <section class="entry-list">
      @while (have_posts())
        @php(the_post())
        @include ('partials.summary-'.(get_post_type() !== 'post' ? get_post_type() : get_post_format()))
      @endwhile
    </section>
    {!! spiral_posts_pagination() !!}
  @endif

  @if (is_home() && !is_paged())
    @php($posts = spiral_get_articles(6))
    @if ($posts->have_posts())
      <header class="entry-header">
        <h1>{{ __('Latest Articles', 'spiral') }}</h1>
      </header>
      <section class="entry-list">
        @while ($posts->have_posts())
          @php($posts->the_post())
          @include ('partials.summary-'.(get_post_type() !== 'post' ? get_post_type() : get_post_format()))
        @endwhile
      </section>
    @endif
  @endif
@endsection
