{{--
  Template Name: Home
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts())
    @php(the_post())

    <article @php(post_class('entry-single'))>
      <header class="home-header">
        <h1>{{ get_the_title() }}</h1>
      </header>

      <div class="entry-body">
        @include('partials.content-'.get_post_type())
      </div>
    </article>
  @endwhile
@endsection
