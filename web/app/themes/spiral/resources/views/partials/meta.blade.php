<div class="meta">
  @if (is_singular())
    @if ('post' === get_post_type() || 'editorial' === get_post_type() || 'review' === get_post_type())
      <div class="meta-time">
        {{ __('Date', 'spiral') }} <time datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time>
      </div>
    @endif

    @if ('post' === get_post_type())
      <div class="meta-categories">
        {{ __('Category', 'spiral') }} {!! get_the_category_list(' , ') !!}
      </div>
    @endif

  @else
    <div class="meta-time">
      <time datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time>
    </div>
  @endif
</div>
