<header class="header-main">
  <div class="wrapper">
    <h1 class="header-logo">
      <a href="{{ esc_url(home_url('/')) }}" aria-label="{{ bloginfo('name') }}">
        <svg aria-hidden="true">
          <use xlink:href="{{ get_stylesheet_directory_uri() }}/assets/images/sprite.svg#logo">
        </svg>
      </a>
    </h1>
  </div>
</header>

@include('partials.menu')
