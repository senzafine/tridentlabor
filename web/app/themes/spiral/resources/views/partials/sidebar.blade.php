<aside id="sidebar" class="sidebar">
  @if (!is_page())
    {{ spiral_google_ad_square() }}
  @endif

  @if (is_singular() && 'page' !== get_post_type())
    <section>
      <h3>{{ __('Latest News', 'spiral') }}</h3>
      @php($posts = spiral_get_latest_news(10))
      @if ($posts->have_posts())
        <ul>
          @while ($posts->have_posts())
            @php($posts->the_post())
            <li><a href="{{ the_permalink() }}">{{ the_title() }}</a></li>
          @endwhile
        </ul>
      @endif
    </section>
  @endif

  @if (is_home())
    <section class="sidebar-comments">
      <h3>{{ __('Latest Comments', 'spiral') }}</h3>
      {{ spiral_sidebar_comments() }}
    </section>
  @endif

  @php(dynamic_sidebar('sidebar-primary'))

  <section class="sidebar-twitter">
    <h3>{{ __('Latest Tweets', 'spiral') }}</h3>
    {{ spiral_twitter_widget() }}
  </section>

  <section class="sidebar-facebook">
    <h3>{{ __('Facebook Timeline', 'spiral') }}</h3>
    {{ spiral_facebook_widget() }}
  </section>

  @if (!is_page())
    <a href="http://www.largeassmovieblogs.com/" target="_blank" class="sidebar-affiliate"><img src="http://www.largeassmovieblogs.com/wp-content/uploads/2012/09/lamb-icon.png" alt="Large Association of Movie Blogs" /></a>
  @endif
</aside>
