@php($posts = spiral_get_featured_posts(5))
@if ($posts->have_posts())
  <div class="swiper">
    <div class="swiper-container">
      <div class="swiper-wrapper">
        @while ($posts->have_posts())
          @php ($posts->the_post())
          @php($title = get_the_title($posts-> ID))
          @if (get_post_type($posts->ID) === 'review')
            @php($title = __('Review', 'spiral') . ': ' . $title)
          @endif
          <div class="swiper-slide">
            <a href="{{ the_permalink() }}">
              <figure>{!! get_the_post_thumbnail($posts->ID, 'cover_image') !!}</figure>
              <figcaption>{{ $title }}</figcaption>
            </a>
          </div>
        @endwhile
      </div>
      <div class="swiper-pagination"></div>
      <div class="swiper-button-next">
        <svg aria-hidden="true" class="icon icon-search">
          <use xlink:href="{{ get_stylesheet_directory_uri() }}/assets/images/sprite.svg#next">
        </svg>
      </div>
      <div class="swiper-button-prev">
        <svg aria-hidden="true" class="icon icon-search">
          <use xlink:href="{{ get_stylesheet_directory_uri() }}/assets/images/sprite.svg#prev">
        </svg>
      </div>
    </div>
  </div>
@endif

@php(wp_reset_postdata())
