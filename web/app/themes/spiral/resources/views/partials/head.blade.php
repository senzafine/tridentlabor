<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, shrink-to-fit=no">
  <title>{{ wp_title('|', true, 'right') }}</title>
  <link rel="canonical" href="{{ bloginfo('url') }}">
  <link rel="apple-touch-icon" sizes="180x180" href="{{ get_stylesheet_directory_uri() }}/assets/images/apple-touch-icon.png?v=2bwaKj097l">
  <link rel="icon" type="image/png" href="{{ get_stylesheet_directory_uri() }}/assets/images/favicon-16x16.png?v=2bwaKj097l" sizes="16x16">
  <link rel="icon" type="image/png" href="{{ get_stylesheet_directory_uri() }}/assets/images/favicon-32x32.png?v=2bwaKj097l" sizes="32x32">
  <link rel="icon" type="image/png" href="{{ get_stylesheet_directory_uri() }}/assets/images/favicon-96x96.png?v=2bwaKj097l" sizes="96x96">
  <link rel="icon" type="image/png" href="{{ get_stylesheet_directory_uri() }}/assets/images/android-chrome-192x192.png?v=2bwaKj097l" sizes="192x192">
  <link rel="manifest" href="{{ get_stylesheet_directory_uri() }}/assets/images/manifest.json?v=2bwaKj097l">
  <link rel="mask-icon" href="{{ get_stylesheet_directory_uri() }}/assets/images/safari-pinned-tab.svg?v=2bwaKj097l" color="#dc1419">
  <link rel="shortcut icon" href="{{ get_stylesheet_directory_uri() }}/assets/images/favicon.ico?v=2bwaKj097l">
  <meta name="apple-mobile-web-app-title" content="{{ bloginfo('name') }}">
  <meta name="application-name" content="{{ bloginfo('name') }}">
  <meta name="msapplication-TileColor" content="#14171e">
  <meta name="msapplication-TileImage" content="{{ get_stylesheet_directory_uri() }}/assets/images/mstile-144x144.png?v=2bwaKj097l">
  <meta name="msapplication-config" content="{{ get_stylesheet_directory_uri() }}/assets/images/browserconfig.xml?v=2bwaKj097l">
  <meta name="theme-color" content="#14171e">
  @php(wp_head())
  {{ spiral_schema() }}
</head>
