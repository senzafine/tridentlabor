<section class="main-navigation">
  <div class="wrapper">
    <div class="main-menu">
      <input type="checkbox" id="toggle-menu" class="toggle-checkbox">
      <label for="toggle-menu" class="toggle-label toggle-menu" aria-label="{{ __('Menu', 'spiral') }}">
        <svg aria-hidden="true" class="icon icon-menu">
          <use xlink:href="{{ get_stylesheet_directory_uri() }}/assets/images/sprite.svg#menu">
        </svg>
        <svg aria-hidden="true" class="icon icon-close">
          <use xlink:href="{{ get_stylesheet_directory_uri() }}/assets/images/sprite.svg#close">
        </svg>
      </label>
      @php
      wp_nav_menu([
        'container'       => false,
        'items_wrap'      => '<nav role="navigation"><ul id="%1$s" class="%2$s">%3$s</ul></nav>',
        'menu'            => __('Main Menu', 'spiral'),
        'theme_location'  => 'main_menu'
      ]);
      @endphp
    </div>

    <div class="main-search">
      <input type="checkbox" id="toggle-search" class="toggle-checkbox">
      <label for="toggle-search" class="toggle-label toggle-search" aria-label="{{ __('Search', 'spiral') }}">
        <svg aria-hidden="true" class="icon icon-search">
          <use xlink:href="{{ get_stylesheet_directory_uri() }}/assets/images/sprite.svg#search">
        </svg>
        <svg aria-hidden="true" class="icon icon-close">
          <use xlink:href="{{ get_stylesheet_directory_uri() }}/assets/images/sprite.svg#close">
        </svg>
      </label>
      @include('partials.searchform')
    </div>
  </div>
</section>
