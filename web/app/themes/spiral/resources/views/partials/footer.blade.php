<footer class="footer-main">
  <div class="wrapper">
    <div class="footer-links">
      @php
      wp_nav_menu([
        'container'       => 'nav',
        'depth'           => 1,
        'items_wrap'      => '<nav role="navigation" class="%2$s"><ul id="%1$s">%3$s</ul></nav>',
        'menu'            => __('Footer Menu', 'spiral'),
        'menu_class'      => 'footer-menu',
        'theme_location'  => 'footer_menu'
      ]);
      @endphp
    </div>

    @php(dynamic_sidebar('sidebar-footer'))

    <div class="credits">
      <div class="credits-logo">
        <svg aria-hidden="true">
          <use xlink:href="{{ get_stylesheet_directory_uri() }}/assets/images/sprite.svg#logo_footer">
        </svg>
      </div>
      <div class="credits-legal">
        <p>&copy; {{ date('Y') }} <strong>{{ bloginfo('name') }}</strong>. {{ __('All Rights Reserved.', 'spiral') }}</p>
      </div>
    </div>
  </div>
</footer>
