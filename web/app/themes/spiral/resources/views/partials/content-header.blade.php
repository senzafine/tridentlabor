@if (!is_home())
  <header class="entry-header">
    <h1>{!! App\title() !!}</h1>
  </header>
@endif
