<?php

/**
 * Do not edit anything in this file unless you know what you're doing
 */

/**
 * Helper function for prettying up errors
 * @param string $message
 * @param string $subtitle
 * @param string $title
 */
$spiral_error = function ($message, $subtitle = '', $title = '') {
    $title = $title ?: __('Spiral Error', 'spiral');
    $footer = '<a href="http://senzafine.net/spiral">senzafine.net/spiral</a>';
    $message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p><p>{$footer}</p>";
    wp_die($message, $title);
};

/**
 * Ensure compatible version of PHP is used
 */
if (version_compare('5.6.4', phpversion(), '>=')) {
    $spiral_error(__('You must be using PHP 5.6.4 or greater.', 'spiral'), __('Invalid PHP version', 'spiral'));
}

/**
 * Ensure compatible version of WordPress is used
 */
if (version_compare('4.7.0', get_bloginfo('version'), '>=')) {
    $spiral_error(__('You must be using WordPress 4.7.0 or greater.', 'spiral'), __('Invalid WordPress version', 'spiral'));
}

/**
 * Ensure dependencies are loaded
 */
if (!class_exists('Senzafine\\Spiral\\Container')) {
    if (!file_exists($composer = __DIR__.'/../vendor/autoload.php')) {
        $spiral_error(
            __('You must run <code>composer install</code> from the Spiral directory.', 'spiral'),
            __('Autoloader not found.', 'spiral')
        );
    }
    require_once $composer;
}

/**
 * Spiral required files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
$includes = [
    'base',
    'setup',
    'filters',
    'login',
    'dashboard',
    'images',
    'titles',
    'posts',
    'breadcrumbs',
    'meta',
    'comments',
    'pagination',
    'shortcodes'
];

array_map(function ($file) use ($spiral_error) {
    $file = "../app/{$file}.php";
    if (!locate_template($file, true, true)) {
        $spiral_error(sprintf(__('Error locating <code>%s</code> for inclusion.', 'spiral'), $file), 'File not found');
    }
}, $includes);

/**
 * Here's what's happening with these hooks:
 * 1. WordPress initially detects theme in themes/spiral
 * 2. Upon activation, we tell WordPress that the theme is actually in themes/spiral/resources/views
 * 3. When we call get_template_directory() or get_template_directory_uri(), we point it back to themes/spiral
 *
 * We do this so that the Template Hierarchy will look in themes/spiral/resources/views for core WordPress themes
 * But functions.php, style.css, and index.php are all still located in themes/spiral
 *
 * This is not compatible with the WordPress Customizer theme preview prior to theme activation
 *
 * get_template_directory()   -> /srv/www/example.com/current/web/app/themes/spiral/resources
 * get_stylesheet_directory() -> /srv/www/example.com/current/web/app/themes/spiral/resources
 * locate_template()
 * ├── STYLESHEETPATH         -> /srv/www/example.com/current/web/app/themes/spiral/resources/views
 * └── TEMPLATEPATH           -> /srv/www/example.com/current/web/app/themes/spiral/resources
 */
if (is_customize_preview() && isset($_GET['theme'])) {
    $spiral_error(__('Theme must be activated prior to using the customizer.', 'spiral'));
}

$spiral_views = basename(dirname(__DIR__)).'/'.basename(__DIR__).'/views';

add_filter('stylesheet', function () use ($spiral_views) {
    return dirname($spiral_views);
});

add_filter('stylesheet_directory_uri', function ($uri) {
    return dirname($uri);
});

if ($spiral_views !== get_option('stylesheet')) {
    update_option('stylesheet', $spiral_views);
    wp_redirect($_SERVER['REQUEST_URI']);
    exit();
}
