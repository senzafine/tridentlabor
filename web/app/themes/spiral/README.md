# [Spiral](https//senzafine.net/spiral/)

Spiral is a WordPress starter theme for modern development forked from Roots Sage.

## Features

* Stylus for stylesheets
* ES6 for JavaScript
* [Webpack](https://webpack.github.io/) for compiling assets, optimizing images, and concatenating and minifying files
* [BrowserSync](http://www.browsersync.io/) for synchronized browser testing
* [Laravel's Blade](https://laravel.com/docs/5.3/blade) as a templating engine

## Requirements

Make sure all dependencies have been installed before moving on:

* [PHP](http://php.net/manual/en/install.php) >= 7.0
* [Composer](https://getcomposer.org/download/)
* [WordPress](https://wordpress.org/) >= 4.7
* [Node.js](http://nodejs.org/) >= 6.9.x

## Theme installation

Clone the git repo:

```
 $ git clone git@gitlab.com:senzafine/spiral.git
```

From the command line on your host machine navigate to the theme directory:

```
 $ composer install && npm install
```

## Theme structure

```shell
themes/spiral/            # → Root of your Spiral based theme
├── app/                  # → Theme PHP
│   ├── core/Spiral/      # → Blade implementation, asset manifest
│   ├── base.php          # → Setup functions
│   ├── dashboard.php     # → Dashboard functions
│   ├── filters.php       # → Theme filters
│   └── setup.php         # → Theme setup
├── assets/               # → Built theme assets (never edit)
├── composer.json         # → Autoloading for `app/` files
├── composer.lock         # → Composer lock file (never edit)
├── node_modules/         # → Node.js packages (never edit)
├── package.json          # → Node.js dependencies and scripts
├── resources/            # → Theme assets and templates
│   ├── sources/          # → Front-end assets
│   │   ├── build/        # → Webpack and ESLint config
│   │   ├── fonts/        # → Theme fonts
│   │   ├── images/       # → Theme images
│   │   ├── scripts/      # → Theme JS
│   │   └── styles/       # → Theme stylesheets
│   ├── views/            # → Theme templates
│   │   ├── layouts/      # → Base templates
│   │   └── partials/     # → Partial templates
│   ├── index.php         # → Never manually edit
│   ├── functions.php     # → Composer autoloader, theme includes
│   ├── screenshot.png    # → Theme screenshot for WP admin
│   └── style.css         # → Theme meta information
└── vendor/               # → Composer packages (never edit)
```

## Theme setup

Edit `app/setup.php` to enable or disable theme features, setup navigation menus, post thumbnail sizes, post formats, and sidebars.

## Theme development

Spiral uses [Webpack](https://webpack.github.io/) as a build tool and [npm](https://www.npmjs.com/) to manage front-end packages.

### Build commands

* `npm start` — Compile assets when file changes are made, start BrowserSync session.
* `npm run build` — Compile and optimize the files in your assets directory for production.
* `npm run stats` - Run stats.
* `npm run lint:scripts` - Lint javascript files.
* `npm run lint:styles` - Lint style files.
* `npm sprite` — Compile svg images from sprites folder.

### Using BrowserSync

To use BrowserSync during `npm start` you need to update `proxy` in the `webpack.config.js` file at the root of the theme to your local development address.

BrowserSync will use webpack's [HMR](https://webpack.github.io/docs/hot-module-replacement.html), which won't trigger a page reload in your browser.

If you would like to force BrowserSync to reload the page whenever certain file types are edited, then add them to the `files` array in `webpack.config.tasks.json`.

```json
...
  files: [
    "app/**/*.php",
    "resources/*.php",
    "resources/assets/scripts/**/*.*",
    "resources/assets/images/**/*.*",
    "resources/views/**/*.php"
  ]
...
```
