<?php

add_action('init', 'register_shortcodes');

function register_shortcodes()
{
    add_shortcode('button', function ($atts) {
        extract(shortcode_atts(
            [
                'link' => '',
                'text' => ''
            ],
            $atts
        ));

        $output = '<a href="' . $link . '" class="button button-call">' . $text . '</a>';

        return $output;
    });
}
