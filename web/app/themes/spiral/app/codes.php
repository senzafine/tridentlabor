<?php

/**
 * Google Adsense
 */
function spiral_google_ad_square()
{
    ?>
    <section class="ad">
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- moviefy Sidebar -->
        <ins class="adsbygoogle"
            style="display:inline-block;width:240px;height:240px"
            data-ad-client="ca-pub-0272241184921278"
            data-ad-slot="1895908605"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
    </section>
    <?php
}

/**
 * Google Analitics
 */
function spiral_google_analytics()
{
    ?>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-26023502-2', 'auto');
        ga('send', 'pageview');
    </script>
    <?php
}

/**
 * Facebook init
 */
function spiral_facebook_sdk()
{
    ?>
    <div id="fb-root"></div>
    <script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=410360185726979";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, "script", "facebook-jssdk"));
    </script>
    <?php
}

/**
 * Twitter widget
 */
function spiral_twitter_widget()
{
    ?>
    <a
     class="twitter-timeline"
     href="https://twitter.com/moviefy"
     data-widget-id="296257881509085184"
     data-height="300"
     data-chrome="noheader noborders transparent"><?php sprintf(__('Tweets by %s', 'spiral'), '@moviefy'); ?></a>
    <script>
        !function(d,s,id) {
            var js,fjs=d.getElementsByTagName(s)[0],
            p=/^http:/.test(d.location)?"http":"https";
            if(!d.getElementById(id)){
                js=d.createElement(s);
                js.id=id;
                js.src=p+"://platform.twitter.com/widgets.js";
                fjs.parentNode.insertBefore(js,fjs);
            }
        } (document,"script","twitter-wjs");
    </script>
    <?php
}

/**
 * Facebook widget
 */
function spiral_facebook_widget()
{
    ?>
    <div class="fb-page"
        data-href="https://www.facebook.com/moviefy/"
        data-tabs="timeline"
        data-width="240"
        data-height="300"
        data-small-header="false"
        data-adapt-container-width="true"
        data-hide-cover="false"
        data-show-facepile="true">
        <blockquote cite="https://www.facebook.com/moviefy/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/moviefy/">moviefy</a></blockquote>
    </div>
    <?php
}
