<?php

/**
 * Define custom post types
 */
add_action('init', function () {
    $movies = [
        'labels' => [
            'name'               => __('Movies', 'spiral'),
            'singular_name'      => __('Movie', 'spiral'),
            'menu_name'          => __('Movies', 'spiral'),
            'name_admin_bar'     => __('Movie', 'spiral'),
            'add_new'            => __('Add Movie', 'spiral'),
            'add_new_item'       => __('Add New Movie', 'spiral'),
            'new_item'           => __('New Movie', 'spiral'),
            'edit_item'          => __('Edit Movie', 'spiral'),
            'view_item'          => __('View Movie', 'spiral'),
            'all_items'          => __('All Movies', 'spiral'),
            'search_items'       => __('Search Movies', 'spiral'),
            'parent_item_colon'  => __('Parent Movie', 'spiral'),
            'not_found'          => __('No Movies Found', 'spiral'),
            'not_found_in_trash' => __('No movies found in trash', 'spiral')
        ],
        'label'                  => __('Movies', 'spiral'),
        'description'            => __('Post type for movie database pages', 'spiral'),
        'public'                 => true,
        'menu_position'          => 4,
        //'menu_icon'            => get_stylesheet_directory_uri() . '/assets/images/icons/icon-movie.png',
        'hierarchical'           => false,
        //'register_meta_box_cb' => 'metabox_movie',
        'can_export'             => true,
        'query_var'              => true,
        'has_archive'            => 'movies',
        'rewrite' => [
            'slug'       => 'movie',
            'with_front' => false
        ],
        'supports' => [
            'title',
            'editor',
            'author',
            'thumbnail',
            'excerpt',
            'custom-fields',
            'comments',
            'revisions',
            'page-attributes'
        ],
        'taxonomies' => [
            'post_tag'
        ]
    ];

    $soundtracks = [
        'labels' => [
            'name'               => __('Soundtracks', 'spiral'),
            'singular_name'      => __('Soundtrack', 'spiral'),
            'menu_name'          => __('Soundtracks', 'spiral'),
            'name_admin_bar'     => __('Soundtrack', 'spiral'),
            'add_new'            => __('Add Soundtrack', 'spiral'),
            'add_new_item'       => __('Add New Soundtrack', 'spiral'),
            'new_item'           => __('New Soundtrack', 'spiral'),
            'edit_item'          => __('Edit Soundtrack', 'spiral'),
            'view_item'          => __('View Soundtrack', 'spiral'),
            'all_items'          => __('Soundtracks', 'spiral'),
            'search_items'       => __('Search Soundtracks', 'spiral'),
            'parent_item_colon'  => __('Parent Soundtrack', 'spiral'),
            'not_found'          => __('No Soundtracks Found', 'spiral'),
            'not_found_in_trash' => __('No soundtracks found in trash', 'spiral')
        ],
        'label'                  => __('Soundtracks', 'spiral'),
        'description'            => __('Post type for soundtrack database pages', 'spiral'),
        'public'                 => true,
        'menu_position'          => 4,
        //'menu_icon'            => get_stylesheet_directory_uri() . '/assets/images/icons/icon-soundtrack.png',
        'hierarchical'           => false,
        //'register_meta_box_cb' => 'metabox_soundtrack',
        'can_export'             => true,
        'query_var'              => true,
        'has_archive'            => 'soundtracks',
        'rewrite' => [
            'slug'       => 'soundtrack',
            'with_front' => false
        ],
        'supports' => [
            'title',
            'editor',
            'author',
            'thumbnail',
            'excerpt',
            'custom-fields',
            'comments',
            'revisions',
            'page-attributes'
        ],
        'taxonomies' => [
            'post_tag'
        ]
    ];

    $reviews = [
        'labels' => [
            'name'               => __('Reviews', 'spiral'),
            'singular_name'      => __('Review', 'spiral'),
            'menu_name'          => __('Reviews', 'spiral'),
            'name_admin_bar'     => __('Review', 'spiral'),
            'add_new'            => __('Add Review', 'spiral'),
            'add_new_item'       => __('Add New Review', 'spiral'),
            'new_item'           => __('New Review', 'spiral'),
            'edit_item'          => __('Edit Review', 'spiral'),
            'view_item'          => __('View Review', 'spiral'),
            'all_items'          => __('Reviews', 'spiral'),
            'search_items'       => __('Search Reviews', 'spiral'),
            'parent_item_colon'  => __('Parent Review', 'spiral'),
            'not_found'          => __('No Reviews Found', 'spiral'),
            'not_found_in_trash' => __('No reviews found in trash', 'spiral')
        ],
        'label'                  => __('Reviews', 'spiral'),
        'description'            => __('Post type for movie reviews', 'spiral'),
        'public'                 => true,
        'menu_position'          => 4,
        //'menu_icon'            => get_stylesheet_directory_uri() . '/assets/images/icons/icon-soundtrack.png',
        'hierarchical'           => false,
        //'register_meta_box_cb' => 'metabox_soundtrack',
        'can_export'             => true,
        'query_var'              => true,
        'has_archive'            => 'reviews',
        'rewrite' => [
            'slug'       => 'review',
            'with_front' => false
        ],
        'supports' => [
            'title',
            'editor',
            'author',
            'thumbnail',
            'excerpt',
            'custom-fields',
            'comments',
            'revisions',
            'page-attributes'
        ],
        'taxonomies' => [
            'post_tag'
        ]
    ];

    $editorials = [
        'labels' => [
            'name'               => __('Editorials', 'spiral'),
            'singular_name'      => __('Editorial', 'spiral'),
            'menu_name'          => __('Editorials', 'spiral'),
            'name_admin_bar'     => __('Editorial', 'spiral'),
            'add_new'            => __('Add Editorial', 'spiral'),
            'add_new_item'       => __('Add New Editorial', 'spiral'),
            'new_item'           => __('New Editorial', 'spiral'),
            'edit_item'          => __('Edit Editorial', 'spiral'),
            'view_item'          => __('View Editorial', 'spiral'),
            'all_items'          => __('Editorials', 'spiral'),
            'search_items'       => __('Search Editorials', 'spiral'),
            'parent_item_colon'  => __('Parent Editorial', 'spiral'),
            'not_found'          => __('No Editorials Found', 'spiral'),
            'not_found_in_trash' => __('No editorials found in trash', 'spiral')
        ],
        'label'                  => __('Editorials', 'spiral'),
        'description'            => __('Post type for special articles', 'spiral'),
        'public'                 => true,
        'menu_position'          => 4,
        //'menu_icon'            => get_stylesheet_directory_uri() . '/assets/images/icons/icon-soundtrack.png',
        'hierarchical'           => false,
        //'register_meta_box_cb' => 'metabox_soundtrack',
        'can_export'             => true,
        'query_var'              => true,
        'has_archive'            => 'editorials',
        'rewrite' => [
            'slug'       => 'editorial',
            'with_front' => false
        ],
        'supports' => [
            'title',
            'editor',
            'author',
            'thumbnail',
            'excerpt',
            'custom-fields',
            'comments',
            'revisions',
            'page-attributes'
        ],
        'taxonomies' => [
            'post_tag'
        ]
    ];

    $galleries = [
        'labels' => [
            'name'               => __('Galleries', 'spiral'),
            'singular_name'      => __('Gallery', 'spiral'),
            'menu_name'          => __('Galleries', 'spiral'),
            'name_admin_bar'     => __('Gallery', 'spiral'),
            'add_new'            => __('Add Gallery', 'spiral'),
            'add_new_item'       => __('Add New Gallery', 'spiral'),
            'new_item'           => __('New Gallery', 'spiral'),
            'edit_item'          => __('Edit Gallery', 'spiral'),
            'view_item'          => __('View Gallery', 'spiral'),
            'all_items'          => __('Galleries', 'spiral'),
            'search_items'       => __('Search Galleries', 'spiral'),
            'parent_item_colon'  => __('Parent Gallery', 'spiral'),
            'not_found'          => __('No Galleries Found', 'spiral'),
            'not_found_in_trash' => __('No galleries found in trash', 'spiral')
        ],
        'label'                  => __('Galleries', 'spiral'),
        'description'            => __('Post type for movie galleries', 'spiral'),
        'public'                 => true,
        'menu_position'          => 4,
        //'menu_icon'            => get_stylesheet_directory_uri() . '/assets/images/icons/icon-soundtrack.png',
        'hierarchical'           => false,
        //'register_meta_box_cb' => 'metabox_soundtrack',
        'can_export'             => true,
        'query_var'              => true,
        'has_archive'            => 'galleries',
        'rewrite' => [
            'slug'       => 'gallery',
            'with_front' => false
        ],
        'supports' => [
            'title',
            'editor',
            'author',
            'thumbnail',
            'excerpt',
            'custom-fields',
            'comments',
            'revisions',
            'page-attributes'
        ],
        'taxonomies' => [
            'post_tag'
        ]
    ];

    register_post_type('movie', $movies);
    register_post_type('soundtrack', $soundtracks);
    register_post_type('review', $reviews);
    register_post_type('editorial', $editorials);
    register_post_type('gallery', $galleries);
});

/**
 * Build metabox for the soundtrack post type
 */
add_action('add_meta_boxes', function () {
    add_meta_box(
        'soundtrack-details',
        'Soundtrack Details',
        'spiral_soundtrack_metabox',
        'soundtrack'
    );
});

function spiral_soundtrack_metabox()
{
    global $wpdb;
    global $post;

    $charset_collate = $wpdb->get_charset_collate();
    $names_table     = $wpdb->prefix . 'names';
    $query_names     = "CREATE TABLE IF NOT EXISTS $names_table (`id` mediumint(9) NOT NULL AUTO_INCREMENT, `name` text NOT NULL, UNIQUE (`id`)) $charset_collate;";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($query_names);

    $details = get_post_meta($post->ID, 'soundtrack', true);
    $artists = $details['artist'];
    $labels  = $details['label'];
    $release = $details['release'];
    $itunes  = $details['itunes'];
    $amazon  = $details['amazon'];
    $buy     = $details['buy'];
    ?>

    <input type="hidden" name="soundtrack_noncename" value="<?= wp_create_nonce(basename(__FILE__)); ?>">

    <label for="soundtrack[artist]"><?= __('Album Artist', 'spiral'); ?>:</label>
    <div class="names">
        <button type="button" class="button button-small name-add">+</button>

        <?php
        if ($artists) {
            foreach ($artists as $key => $artist) {
                $artist_name = $wpdb->get_var($wpdb->prepare("SELECT name FROM {$names_table} WHERE id=%s", $artist));

                if ($artist_name && !empty($artist_name)) {
                    $artist = $artist_name;
                } else {
                    $artist = $artist;
                }
                ?>

                <span class="name">
                    <input type="text" name="soundtrack[artist][]" value="<?= $artist; ?>" class="input-names">
                    <button type="button" class="button button-small name-remove">x</button>
                </span>

                <?php
            }
        } else {
            ?>

            <span class="name">
                <input type="text" name="soundtrack[artist][]" class="input-names">
                <button type="button" class="button button-small name-remove">x</button>
            </span>
            <?php
        }
        ?>

    </div>

    <label for="soundtrack[label]"><?= __('Record Label', 'spiral'); ?>:</label>
    <div class="names">
        <button type="button" class="button button-small name-add">+</button>

        <?php
        if ($labels) {
            foreach ($labels as $key => $label) {
                $label_name = $wpdb->get_var($wpdb->prepare("SELECT name FROM {$names_table} WHERE id=%s", $label));

                if ($label_name && !empty($label_name)) {
                    $label = $label_name;
                } else {
                    $label = $label;
                }
                ?>

                <span class="name">
                    <input type="text" name="soundtrack[label][]" value="<?= $label; ?>" class="input-names">
                    <button type="button" class="button button-small name-remove">x</button>
                </span>

                <?php
            }
        } else {
            ?>

            <span class="name">
                <input type="text" name="soundtrack[label][]" class="input-names">
                <button type="button" class="button button-small name-remove">x</button>
            </span>

            <?php
        }
        ?>

    </div>

    <label for="soundtrack[release]"><?= __('Release Date', 'spiral'); ?>:</label>
    <input type="date" name="soundtrack[release]" value="<?= $details['release']; ?>">
    <label for="soundtrack[itunes]"><?= __('iTunes Link', 'spiral'); ?>:</label>
    <input type="text" name="soundtrack[itunes]" value="<?= $details['itunes']; ?>">
    <label for="soundtrack[amazon]"><?= __('Amazon Link', 'spiral'); ?>:</label>
    <input type="text" name="soundtrack[amazon]" value="<?= $details['amazon']; ?>">
    <label for="soundtrack[buy]"><?= __('Buy Link', 'spiral'); ?>:</label>
    <input type="text" name="soundtrack[buy]" value="<?= $details['buy']; ?>">

    <?php
}

/**
 * Build metabox for tracklists
 */
add_action('add_meta_boxes', function () {
    add_meta_box(
        'soundtrack-tracklist',
        'Soundtrack Tracklist',
        'spiral_tracklist_metabox',
        'soundtrack'
    );
});

function spiral_tracklist_metabox()
{
    global $wpdb;
    global $post;

    $tracks = get_post_meta($post->ID, 'tracklist', true);
    ?>

    <input type="hidden" name="tracklist_noncename" value="<?= wp_create_nonce(basename(__FILE__)); ?>">

    <table class="soundtrack-tracklist">
        <thead>
            <tr>
                <th>#</th>
                <th><?= __('Name', 'spiral'); ?></th>
                <th><?= __('Artist', 'spiral'); ?></th>
                <th><?= __('Minutes', 'spiral'); ?></th>
                <th><?= __('Seconds', 'spiral'); ?></th>
                <th><?= __('Disc', 'spiral'); ?></th>
            </tr>
        </thead>
        <tbody>

            <?php
            if ($tracks && !empty($tracks)) {
                foreach ($tracks as $track) {
                    ?>

                    <tr class="soundtrack-track">
                        <td class="soundtrack-number"><input type="number" name="track[][number]" value="<?= $track['number']; ?>"></td>
                        <td class="soundtrack-name"><input type="text" name="track[][name]" value="<?= $track['name']; ?>"></td>
                        <td class="soundtrack-artist"><input type="text" name="track[][artist]" value="<?= $track['artist']; ?>" class="input-names"></td>
                        <td class="soundtrack-minutes"><input type="number" name="track[][minutes]" value="<?= $track['minutes']; ?>"></td>
                        <td class="soundtrack-seconds"><input type="number" name="track[][seconds]" value="<?= $track['seconds']; ?>" max="59"></td>
                        <td class="soundtrack-disc"><input type="number" name="track[][disc]" value="<?= $track['disc']; ?>"></td>
                        <td class="soundtrack-buttons"><button type="button" class="button button-small track-remove"><?= __('Remove', 'spiral'); ?></button></td>
                    </tr>

                    <?php
                    $count++;
                }
            } else {
                ?>

                <tr class="soundtrack-track">
                    <td class="soundtrack-number"><input type="number" name="track[][number]"></td>
                    <td class="soundtrack-name"><input type="text" name="track[][name]"></td>
                    <td class="soundtrack-artist"><input type="text" name="track[][artist]" class="input-names"></td>
                    <td class="soundtrack-minutes"><input type="number" name="track[][minutes]"></td>
                    <td class="soundtrack-seconds"><input type="number" name="track[][seconds]" max="59"></td>
                    <td class="soundtrack-disc"><input type="number" name="track[][disc]"></td>
                    <td class="soundtrack-buttons"><button type="button" class="button button-small track-remove"><?= __('Remove', 'spiral'); ?></button></td>
                </tr>

                <?php
            }
            ?>

        </tbody>
    </table>

    <button type="button" class="button track-add"><?= __('Add Track', 'spiral'); ?></button>
    <button type="button" class="button track-view"><?= __('Add Track', 'spiral'); ?></button>

    <?php
}

/**
 * Save the custom post meta
 */
add_action('save_post_soundtrack', function ($post_id) {
    global $wpdb;

    $names_table = $wpdb->prefix . 'names';

    if (!isset($_POST['soundtrack_noncename']) || !wp_verify_nonce($_POST['soundtrack_noncename'], basename(__FILE__))) {
        return $post_id;
    }

    if (!isset($_POST['tracklist_noncename']) || !wp_verify_nonce($_POST['tracklist_noncename'], basename(__FILE__))) {
        return $post_id;
    }

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }

    if ('soundtrack' === $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id)) {
            return $post_id;
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
        }
    }

    $details = $_POST['soundtrack'];
    $artists = $details['artist'];
    $labels  = $details['label'];

    if ($artists && !empty($artists)) {
        foreach ($artists as $key => $artist) {
            $artist_id = $wpdb->get_var($wpdb->prepare("SELECT id FROM {$names_table} WHERE name=%s", $artist));

            if (! $artist_id || empty($artist_id)) {
                $wpdb->insert(
                    $names_table,
                    [
                        'name' => $artist
                    ]
                );

                $artist_id = $wpdb->insert_id;
            }

            if ($artist_id && ! empty($artist_id)) {
                $artists[$key] = $artist_id;
            }
        }
    }

    if ($labels && ! empty($labels)) {
        foreach ($labels as $key => $label) {
            $label_id = $wpdb->get_var($wpdb->prepare("SELECT id FROM {$names_table} WHERE name=%s", $label));

            if (! $label_id || empty($label_id)) {
                $wpdb->insert(
                    $names_table,
                    [
                        'name' => $label
                    ]
                );

                $label_id = $wpdb->insert_id;
            }

            if ($label_id && !empty($label_id)) {
                $labels[$key] = $label_id;
            }
        }
    }

    foreach ($details as $key => $value) {
        if ('artist' === $key) {
            $details[$key] = $artists;
        } elseif ('label' === $key) {
            $details[$key] = $labels;
        }
    }

    update_post_meta($post_id, 'soundtrack', $details);

    $counter = 0;
    $keys = 0;

    foreach ($_POST['track'] as $key => $value) {
        if (array_key_exists('number', $value)) {
            $tracklist[$counter][number] = $value[number];
        } elseif (array_key_exists('name', $value)) {
            $tracklist[$counter][name] = $value[name];
        } elseif (array_key_exists('artist', $value)) {
            $tracklist[$counter][artist] = $value[artist];
        } elseif (array_key_exists('minutes', $value)) {
            $tracklist[$counter][minutes] = $value[minutes];
        } elseif (array_key_exists('seconds', $value)) {
            $tracklist[$counter][seconds] = $value[seconds];
        } elseif (array_key_exists('disc', $value)) {
            $tracklist[$counter][disc] = $value[disc];
        }

        $keys++;

        if (6 === $keys) {
            $keys = 0;
            $counter++;
        }
    }

    update_post_meta($post_id, 'tracklist', $tracklist);
});

/**
 * Short code for formating tracklists
 */
add_shortcode('tracklist', function ($atts, $content = null) {
    $tracklist = '<table class="soundtrack-tracklist">';
    $tracklist .= '<thead><tr><th>';
    $tracklist .= __('Name', 'spiral');
    $tracklist .= '</th></tr></thead>';
    $tracklist .= '<tr><td>';
    $content = strip_tags($content, '<ol><li>');
    $tracklist .= $content;
    $tracklist .= '</tr></td>';
    $tracklist .= '</table>';

    return $tracklist;
});

/**
 * Add Publicize to custom post types
 */
add_action('init', function () {
    add_post_type_support('editorial', 'publicize');
    add_post_type_support('review', 'publicize');
    add_post_type_support('movie', 'publicize');
    add_post_type_support('soundtrack', 'publicize');
    add_post_type_support('gallery', 'publicize');
});

/**
 * Add WP short lin support to custom post types
 */
add_action('init', function () {
    add_post_type_support('editorial', 'shortlinks');
    add_post_type_support('review', 'shortlinks');
    add_post_type_support('movie', 'shortlinks');
    add_post_type_support('soundtrack', 'shortlinks');
    add_post_type_support('gallery', 'shortlinks');
});

/**
 * Get names from the database
 */
add_action('wp_ajax_nopriv_spiral_get_names', 'spiral_get_names');
add_action('wp_ajax_spiral_get_names', 'spiral_get_names');

function spiral_get_names()
{
    global $wpdb;

    $table = $wpdb->prefix . 'names';
    $name  = $wpdb->esc_like(stripslashes($_REQUEST['q'])) . '%';
    $query = "SELECT name FROM {$table} WHERE name LIKE %s";
    $query = $wpdb->prepare($query, $name);

    foreach ($wpdb->get_results($query) as $row) {
        $name = $row->name;
        echo $name;
    }

    die();
}
