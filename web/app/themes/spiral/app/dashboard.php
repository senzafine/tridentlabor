<?php

namespace App;

/**
 * Change greeting message
 */
/*
add_filter('admin_bar_menu', function ($wp_admin_bar) {
    $account = $wp_admin_bar->get_node('my-account');
    $title = str_replace('Howdy,', 'Hi,', $account->title);

    $wp_admin_bar->add_node(
        [
            'title' => $title,
        ]
    );
}, 25);
*/

/**
 * Change footer text
 */
add_filter('admin_footer_text', function () {
    echo 'Made by <a href="http://senzafine.net">Senzafine</a>. Powered by <a href="http://wordpres.org">WordPress</a>.';
});

/**
 * Replace Admin Logo
 */
add_action('admin_head', function () {
    echo '
    <style type="text/css">
      #header-logo {
        background-image: url(' . get_bloginfo('template_directory') . '/assets/images/logo.svg) !important;
      }
    </style>';
});

/**
 * Delays RSS Publishing Feed
 */
add_filter('posts_where', function ($where) {
    global $wpdb;

    if (is_feed()) {
        $now    = gmdate('Y-m-d H:i:s');
        $wait   = '15';
        $device = 'MINUTE';
        $where  .= " AND TIMESTAMPDIFF($device, $wpdb->posts.post_date_gmt, '$now') > $wait ";
    }

    return $where;
});

/**
 * Featured post metabox
 */
add_action('add_meta_boxes', function () {
    add_meta_box('featured_metabox', __('Featured Post', 'spiral'), function () {
        global $post;

        $meta = get_post_meta($post->ID, 'featured_post', true);

        if ('featured' === $meta) {
            $checked = 'checked';
        } else {
            $checked = '';
        }
        ?>

        <input type="hidden" name="featured_metabox_nonce" value="<?= wp_create_nonce(basename(__FILE__)); ?>">
        <label for="featured_post">
            <input type="checkbox" name="featured_post" value="featured" <?php $checked; ?>>
            <?= __('Make this a featured post', 'spiral'); ?>
        </label>
        <legend><small><em><?= __('Show this post in the front page showcase slider', 'spiral'); ?></em></small></legend>
        <?php
    });
});



add_action('save_post', function ($post_id) {
    if (!wp_verify_nonce($_POST['featured_metabox_nonce'], basename(__FILE__))) {
        return $post_id;
    }

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }

    if ('post' === $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id)) {
            return $post_id;
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
        }
    }

    $data_new = $_POST['featured_post'];

    if (!empty($data_new)) {
        update_post_meta($post_id, 'featured_post', $data_new);
    } else {
        delete_post_meta($post_id, 'featured_post');
    }
});


/**
 * Theme customizer
 */
add_action('customize_register', function (\WP_Customize_Manager $wp_customize) {
    // Add postMessage support
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial('blogname', [
        'selector' => '.brand',
        'render_callback' => function () {
            bloginfo('name');
        }
    ]);
});

/**
 * Customizer JS
 */
add_action('customize_preview_init', function () {
    wp_enqueue_script('spiral/customizer.js', asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
});
