<?php

/**
 * Remove default image sizes
 */
add_filter('intermediate_image_sizes_advanced', function ($sizes) {
    //unset($sizes['medium']);
    unset($sizes['medium']);
    unset($sizes['medium_large']);
    unset($sizes['large']);

    return $sizes;
});

/**
 * Define default image sizes
 */
add_image_size('thumbnail', 240, 240, true);
add_image_size('featured_image_thumbnail', 480, 240, true);
add_image_size('featured_image', 960, 480, true);
add_image_size('cover_image', 1200, 400, true);
add_image_size('opengraph_image', 600, 315, true);

/**
 * Define names for custom image sizes
 */
add_filter('image_size_names_choose', function ($sizes) {
    return array_merge($sizes, [
        'thumbnail'                => __('Thumbnail', 'spiral'),
        'featured_image_thumbnail' => __('Featured Thumbnail', 'spiral'),
        'featured_image'           => __('Featured', 'spiral'),
        'cover_image'              => __('Cover', 'spiral'),
        'opengraph_image'          => __('Opengraph', 'spiral')
    ]);
});

/**
 * Force image resize even if the requested size is bigger than the image
 */
add_filter('image_resize_dimensions', function ($default, $orig_w, $orig_h, $new_w, $new_h, $crop) {
    if (!$crop) {
        return null;
    }

    $aspect_ratio = $orig_w / $orig_h;
    $size_ratio   = max($new_w / $orig_w, $new_h / $orig_h);
    $crop_w       = round($new_w / $size_ratio);
    $crop_h       = round($new_h / $size_ratio);
    $s_x          = floor(($orig_w - $crop_w) / 2);
    $s_y          = floor(($orig_h - $crop_h) / 2);

    return array(0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h);
}, 10, 6);

/**
 * Clean Media Library output
 */
add_shortcode('wp_caption', 'spiral_clean_caption_shortcode');
add_shortcode('caption', 'spiral_clean_caption_shortcode');

function spiral_clean_caption_shortcode($attr, $content = null)
{
    if (! isset($attr['caption'])) {
        if (preg_match('#((?:<a [^>]+>s*)?<img [^>]+>(?:s*</a>)?)(.*)#is', $content, $matches)) {
            $content         = $matches[1];
            $attr['caption'] = trim($matches[2]);
        }
    }

    $output = apply_filters('img_caption_shortcode', '', $attr, $content);

    if (! empty($output)) {
        return $output;
    }

    extract(shortcode_atts([
        'id'      => '',
        'align'   => 'alignnone',
        'width'   => '',
        'caption' => ''
    ], $attr));

    if (1 > (int) $width || empty($caption)) {
        return $content;
    }

    if ($id) {
        $id = 'id="' . esc_attr($id) . '" ';
    }

    return '<figure ' . $id . '>' . do_shortcode($content) . '<figcaption>' . $caption . '</figcaption></figure>';
}

/**
 * Remove attributes for uploaded images
 */
add_filter('post_thumbnail_html', 'spiral_remove_image_size_attribute', 10);
add_filter('image_send_to_editor', 'spiral_remove_image_size_attribute', 10);

function spiral_remove_image_size_attribute($html)
{
    $html = preg_replace('/(width|height)="d*"s/', "", $html);
    return $html;
}

/**
 * Show featured images in the dashboard
 */
add_filter('manage_posts_columns', 'spiral_featured_image_column', 5);
add_filter('manage_movies_columns', 'spiral_featured_image_column', 5);
add_filter('manage_soundtracks_columns', 'spiral_featured_image_column', 5);
add_filter('manage_reviews_columns', 'spiral_featured_image_column', 5);
add_filter('manage_editorials_columns', 'spiral_featured_image_column', 5);
add_filter('manage_galleries_columns', 'spiral_featured_image_column', 5);
add_filter('manage_pages_columns', 'spiral_featured_image_column', 5);

function spiral_featured_image_column($columns)
{
    $columns['featured_image'] = __('Image', 'spiral');
    return $columns;
}

add_action('manage_posts_custom_column', 'spiral_show_featured_image', 5);
add_action('manage_movies_custom_column', 'spiral_show_featured_image', 5);
add_action('manage_soundtracks_custom_column', 'spiral_show_featured_image', 5);
add_action('manage_reviews_custom_column', 'spiral_show_featured_image', 5);
add_action('manage_editorials_custom_column', 'spiral_show_featured_image', 5);
add_action('manage_galleries_custom_column', 'spiral_show_featured_image', 5);
add_action('manage_pages_custom_column', 'spiral_show_featured_image', 5);

function spiral_show_featured_image($column)
{
    if ('featured_image' === $column) {
        if (function_exists('the_post_thumbnail')) {
            echo the_post_thumbnail('thumbnail');
        }
    }
}

add_filter('manage_posts_columns', 'spiral_columns_order');
add_filter('manage_movies_columns', 'spiral_columns_order');
add_filter('manage_soundtracks_columns', 'spiral_columns_order');
add_filter('manage_reviews_columns', 'spiral_columns_order');
add_filter('manage_editorials_columns', 'spiral_columns_order');
add_filter('manage_galleries_columns', 'spiral_columns_order');
add_filter('manage_pages_columns', 'spiral_columns_order');

function spiral_columns_order($columns)
{
    $columns_new = array();
    $thumbs = $columns['featured_image'];
    unset($columns['featured_image']);

    foreach ($columns as $key => $value) {
        if ('title' == $key) {
            $columns_new['featured_image'] = $thumbs;
        }
        $columns_new[$key] = $value;
    }

    return $columns_new;
}
