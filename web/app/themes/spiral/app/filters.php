<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-display';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", function ($templates) {
        return collect($templates)->flatMap(function ($template) {
            $transforms = [
                '%^/?(resources[\\/]views)?[\\/]?%' => '',
                '%(\.blade)?(\.php)?$%' => ''
            ];
            $normalizedTemplate = preg_replace(array_keys($transforms), array_values($transforms), $template);
            return ["{$normalizedTemplate}.blade.php", "{$normalizedTemplate}.php"];
        })->toArray();
    });
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("spiral/template/{$class}/data", $data, $template);
    }, []);
    echo template($template, $data);
    // Return a blank file to make WordPress happy
    return get_theme_file_path('index.php');
}, PHP_INT_MAX);

/**
 * Tell WordPress how to find the compiled path of comments.blade.php
 */
add_filter('comments_template', 'App\\template_path');

/**
 * Get the post excerpt
 */
add_filter('wp_trim_excerpt', function ($text, $raw_excerpt = '') {
    /** Get the post object to be able to get the data from it */
    global $post;

    $post    = get_post($post);
    $excerpt = $post->post_excerpt;

    /** If it doesn't exist build the excerpt from the first sentence of content*/
    if (!$raw_excerpt) {
        $excerpt = apply_filters('the_content', $post->post_content);
        $excerpt = preg_replace("/<img[^>]+\>/i", "", $excerpt);
        $excerpt = preg_replace("/<h(.*?)\b[^>]*>(.*?)<\/h(.*?)>/i", "", $excerpt);
        $excerpt = substr($excerpt, 0, strpos($excerpt, '</p>') + 4);
        $excerpt = substr($excerpt, 0, 150);
        $excerpt = strip_tags($excerpt);
    } else {
        $excerpt = $raw_excerpt;
    }

    return $excerpt;
}, 10, 2);

/**
 * Add post excerpt to the content
 */
/*
add_filter('the_content', function ($content) {
    if (is_singular() && 'soundtrack' !== get_post_type() && 'gallery' !== get_post_type()) {
        $excerpt = '<p class="big">' . get_the_excerpt($post->ID) . '</p>';
        $content = $excerpt . $content;

        return $content;
    } else {
        return $content;
    }
});
*/

/**
 * Add featured image to the content
 */
add_filter('the_content', function ($content) {
    global $post;

    if (is_singular() && has_post_thumbnail() && 'image' !== get_post_format() && 'gallery' !== get_post_format() && 'video' !== get_post_format() && 'soundtrack' !== get_post_type() && 'gallery' !== get_post_type()) {
        $paged = get_query_var('page') ? get_query_var('page') : false;

        if ($paged == false) {
            $thumbnail = '<figure class="entry-thumbnail">';
            $thumbnail .= get_the_post_thumbnail($post->ID, 'featured_image');
            $thumbnail .= '</figure>';
            $content = $thumbnail . $content;
        }
    }

    return $content;
});

/**
 * Change default avatar
 */
add_filter('avatar_defaults', function ($avatar_defaults) {
    $avatar = get_bloginfo('template_directory') . '/assets/images/avatar.svg';
    $avatar_defaults[$avatar] = "Spiral Avatar";

    return $avatar_defaults;
});

/**
 * Add featured imageto RSS feed
 */
add_filter('the_excerpt_rss', 'spiral_rss_featured_image');
add_filter('the_content_feed', 'spiral_rss_featured_image');

function spiral_rss_featured_image($content)
{
    global $post;

    if (has_post_thumbnail($post->ID)) {
        $content = '<p>' . get_the_post_thumbnail($post->ID, 'opengraph_image') . '</p>' . get_the_content();
    }

    return $content;
}

/**
 * Custom language switcher
 */
add_filter('wp_nav_menu_items', function ($items, $args) {
    $languages = apply_filters('wpml_active_languages', null, 'skip_missing=0');

    if ($languages && $args->theme_location == 'footer_menu') {
        if (!empty($languages)) {
            foreach ($languages as $l) {
                if (!$l['active']) {
                    $items = $items . '<li class="menu-item"><a href="' . $l['url'] . '">' . $l['native_name'] . '</a></li>';
                }
            }
        }
    }

    return $items;
}, 10, 2);
