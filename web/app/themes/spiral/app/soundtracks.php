<?php

/**
 * Get the soundtrack details
 */
function spiral_soundtrack_details($post)
{
    global $wpdb;
    $names_table    = $wpdb->prefix . 'names';
    $data           = get_post_meta($post, 'soundtrack', true);

    $artists        = $data['artist'];
    $artist_name    = '';
    $album_artist   = '';
    $labels         = $data['label'];
    $release        = $data['release'];
    $itunes         = $data['itunes'];
    $amazon         = $data['amazon'];
    $buy            = $data['buy'];
    $artists_lenght = count($data['artist']);
    $labels_lenght  = count($data['label']);

    if ($artists) {
        foreach ($artists as $key => $artist) {
            $artist_name  = $wpdb->get_var($wpdb->prepare("SELECT name FROM {$names_table} WHERE id=%s", $artist));
            $album_artist .= $artist_name;

            if ($key < ($artists_lenght - 1)) {
                $album_artist .= ', ';
            }
        }
    }

    if ($labels) {
        foreach ($labels as $key => $label) {
            $label_name  = $wpdb->get_var($wpdb->prepare("SELECT name FROM {$names_table} WHERE id=%s", $label));
            $album_label .= $label_name;

            if ($key < ($labels_lenght - 1)) {
                $album_label .= ', ';
            }
        }
    }

    $details = [
        'artist'  => $album_artist,
        'label'   => $album_label,
        'release' => $data['release'],
        'itunes'  => $data['itunes'],
        'amazon'  => $data['amazon'],
        'buy'     => $data['buy']
    ];

    return $details;
}

/**
 * Build the tracklist
 */
function spiral_soundtrack_tracklist($tracks)
{
    $output = '<table class="soundtrack-tracklist">';
    $output .= '<thead>';
    $output .= '<tr>';
    $output .= '<th class="track-number"></th>';
    $output .= '<th class="track-name">' . __('Name', 'spiral') . '</th>';
    $output .= '<th class="track-length">' . __('Length', 'spiral') . '</th>';
    $output .= '</tr>';
    $output .= '</thead>';
    $output .= '<tbody>';

    $total_minutes      = '';
    $total_seconds      = '';
    $total_length       = '';
    $length             = '';
    $disc               = 1;
    $disc_tracks        = array();
    $disc_minutes       = array();
    $disc_seconds       = array();
    $disc_length        = array();
    $seconds_to_minutes = '';
    $seconds_to_seconds = '';

    foreach ($tracks as $track) {
        $number  = $track['number'];
        $name    = $track['name'];
        $artist  = $track['artist'];
        $minutes = $track['minutes'];
        $seconds = $track['seconds'];
        $disc    = $track['disc'];

        if (empty($disc)) {
            $disc = 1;
        }

        if ($seconds || !empty($seconds)) {
            if (empty($minutes)) {
                $minutes = 0;
            }

            $total_minutes = $total_minutes + $minutes;
            $total_seconds = $total_seconds + $seconds;
            $disc_minutes[$disc] = $disc_minutes[$disc] + $minutes;
            $disc_seconds[$disc] = $disc_seconds[$disc] + $seconds;

            if (1 === strlen($seconds)) {
                $seconds = 0 . $seconds;
            }

            $length = $minutes . ':' . $seconds;
        }

        $disc_tracks[$disc] .= '<tr>';
        $disc_tracks[$disc] .= '<td class="track-number">' . $number . '</td>';
        $disc_tracks[$disc] .= '<td class="rack-name">';
        $disc_tracks[$disc] .= $name;

        if ($artist && !empty($artist) && $artist !== $album_artist) {
            $disc_tracks[$disc] .= '<span class="track-artist">' . $artist . '</span>';
        }

        $disc_tracks[$disc] .= '</td>';
        $disc_tracks[$disc] .= '<td class="track-length">' . $length . '</td>';
        $disc_tracks[$disc] .= '</tr>';
    }

    $total_discs = count($disc_tracks);

    for ($counter = 1; $counter <= $total_discs; $counter++) {
        $seconds_to_minutes = floor($disc_seconds[$counter] / 60);
        $seconds_to_seconds = $disc_seconds[$counter] % 60;

        if (strlen($total_seconds) === 1) {
            $total_seconds = 0 . $total_seconds;
        }

        if (strlen($seconds_to_seconds) === 1) {
            $seconds_to_seconds = 0 . $seconds_to_seconds;
        }

        $total_length = $disc_minutes[$counter] + $seconds_to_minutes . ':' . $seconds_to_seconds;
        $output .= $disc_tracks[$counter];

        if ($total_discs > 1) {
            $output .= '<tr class="disc-length">';
            $output .= '<th></th>';
            $output .= '<th>' . __('Disc Lenght', 'spiral') . '</th>';
            $output .= '<th class="track-length">' . $total_length . '</th>';
            $output .= '</tr>';
        }
    }

    $output .= '</tbody>';

    $seconds_to_minutes = floor($total_seconds / 60);
    $seconds_to_seconds = $total_seconds % 60;

    if (strlen($seconds_to_seconds) === 1) {
        $seconds_to_seconds = 0 . $seconds_to_seconds;
    }

    $total_length = $total_minutes + $seconds_to_minutes . ':' . $seconds_to_seconds;

    if ('0:00' !== $total_length) {
        $output .= '<tfoot>';
        $output .= '<tr class="disc-length">';
        $output .= '<th></th>';
        $output .= '<th class="soundtrack-length">' . __('Album Lenght', 'spiral') . '</th>';
        $output .= '<th class="track-length">' . $total_length . '</th>';
        $output .= '</tr>';
        $output .= '</tfoot>';
    }

    $output .= '</table>';

    echo $output;
}
