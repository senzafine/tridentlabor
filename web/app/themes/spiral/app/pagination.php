<?php

/**
 * Custom post navigation
 */
function spiral_posts_pagination()
{
    $arguments  = '';
    $navigation = '';
    $output     = '';

    if ($GLOBALS['wp_query']->max_num_pages > 1) {
        $arguments = wp_parse_args($arguments, [
            'prev_text'          => __('Previous Posts', 'spiral'),
            'next_text'          => __('Next Posts', 'spiral'),
            'screen_reader_text' => __('Posts Navigation', 'spiral'),
        ]);

        $next_link          = get_previous_posts_link($arguments['next_text']);
        $prev_link          = get_next_posts_link($arguments['prev_text']);
        $screen_reader_text = $arguments['screen_reader_text'];

        if ($prev_link) {
            $navigation .= '<div class="posts-prev">';
            $navigation .= '<svg class="icon">';
            $navigation .= '<use xlink:href="' . get_stylesheet_directory_uri() .'/assets/images/sprite.svg#prev"></use>';
            $navigation .= '</svg>';
            $navigation .= $prev_link;
            $navigation .= '</div>';
        }

        if ($next_link) {
            $navigation .= '<div class="posts-next">';
            $navigation .= $next_link;
            $navigation .= '<svg class="icon">';
            $navigation .= '<use xlink:href="' . get_stylesheet_directory_uri() .'/assets/images/sprite.svg#next"></use>';
            $navigation .= '</svg>';
            $navigation .= '</div>';
        }

        $output = '<nav role="navigation" class="pagination">';
        $output .= '<h1 class="screen-reader-text">' . $screen_reader_text . '</h1>';
        $output .= $navigation;
        $output .= '</nav>';
    }

    return $output;
}

/**
 * Multipage post navigation
 */
function spiral_post_pagination($arguments = '')
{
    global $page, $numpages, $multipage, $more;

    $defaults = [
        'before'           => '<nav class="pagination">',
        'after'            => '</nav>',
        'link_before'      => '',
        'link_after'       => '',
        'next_or_number'   => 'number',
        'separator'        => ' ',
        'nextpagelink'     => __('Next Page', 'spiral'),
        'previouspagelink' => __('Previous Page', 'spiral'),
        'pagelink'         => '%',
        'echo'             => 1
    ];

    $params             = wp_parse_args($arguments, $defaults);
    $r                  = apply_filters('wp_link_pages_args', $params);
    $screen_reader_text = __('Posts Navigation', 'spiral');
    $output             = '';

    if ($multipage) {
        if ('number' == $r['next_or_number']) {
            $output .= $r['before'];
            $output .= '<h1 class="screen-reader-text">' . $screen_reader_text . '</h1>';
            $prev = $page - 1;
            $output .= '<div class="post-page-prev">';

            if ($prev > 0) {
                $link = _wp_link_page($prev) . $r['link_before'] . $r['previouspagelink'] . $r['link_after'] . '</a>';
                $output .= '<svg class="icon"><use xlink:href="' . get_stylesheet_directory_uri() .'/assets/images/sprite.svg#prev"></use></svg>';
                $output .= apply_filters('wp_link_pages_link', $link, $prev);
            }

            $output .= '</div>';
            $next = $page + 1;
            $output .= '<div class="post-page-numbers">';

            for ($i = 1; $i <= $numpages; $i++) {
                $link = $r['link_before'] . str_replace('%', $i, $r['pagelink']) . $r['link_after'];

                if ($i != $page || ! $more && 1 == $page) {
                    $link = _wp_link_page($i) . $link . '</a>';
                }

                $link = apply_filters('wp_link_pages_link', $link, $i);
                $output .= (1 === $i) ? ' ' : $r['separator'];
                $output .= '<div class="post-page">'. $link . '</div>';
            }

            $output .= '</div>';
            $output .= '<div class="post-page-next">';

            if ($next <= $numpages) {
                if ($prev) {
                    $output .= $r['separator'];
                }

                $link = _wp_link_page($next) . $r['link_before'] . $r['nextpagelink'] . $r['link_after'] . '</a>';
                $output .= apply_filters('wp_link_pages_link', $link, $next);
                $output .= '<svg class="icon"><use xlink:href="' . get_stylesheet_directory_uri() . '/assets/images/sprite.svg#next"></use></svg>';
            }

            $output .= '</div>';
            $output .= $r['after'];
        }
    }

    $html = apply_filters('wp_link_pages', $output, $arguments);

    if ($r['echo']) {
        echo $html;
    }

    return $html;
}

/**
 * Check for multipage post
 */
function spiral_is_paginated()
{
    global $multipage;
    return 0 !== $multipage;
}
