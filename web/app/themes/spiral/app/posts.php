<?php

/**
 * Exclude categories from the main loop on the frontpage
 */
add_action('pre_get_posts', function ($query) {
    if ($query->is_home() && $query->is_main_query()) {
        /** Exclude the `features` category */
        $query->query_vars['cat'] = -106;
    }
});

/**
 * Get posts
 */
function spiral_get_posts($args)
{
    global $post;

    $posts = new WP_Query($args);

    return $posts;
}

/**
 * Get posts from different queries
 */
function spiral_get_articles($number)
{
    if (empty($number)) {
        $posts_number = -1;
    } else {
        $posts_number = $number;
    }

    $custom_posts = get_posts([
        'fields'         => 'ids',
        'post_type'      => ['editorial','review'],
        'posts_per_page' => $posts_number
    ]);

    $features_posts = get_posts([
        'category_name'  => 'features',
        'fields'         => 'ids',
        'post_type'      => 'post',
        'posts_per_page' => $posts_number
    ]);

    $posts  = array_merge($custom_posts, $features_posts);

    $args = [
        'orderby'        => 'date',
        'post__in'       => $posts,
        'post_type'      => ['editorial','post','review'],
        'posts_per_page' => $posts_number
    ];

    $posts = spiral_get_posts($args);

    return $posts;
}

/**
 * Get recent posts
 */
function spiral_get_latest_news($number = 10, $category = 2)
{
    $args = [
        'category_in'    => $category,
        'post_type'      => 'post',
        'posts_per_page' => $number
    ];

    $posts = spiral_get_posts($args);

    return $posts;
}

/**
 * Get featured posts
 */
function spiral_get_featured_posts($number = 5)
{
    $args = [
        'meta_key'       => 'featured_post',
        'meta_value'     => 'featured',
        'posts_per_page' => 5,
        'post_type'      => 'any',
        'post_status'    => 'publish'
    ];

    $posts = spiral_get_posts($args);

    return $posts;
}
