<?php

/**
 * Build metatags
 */
add_action('wp_head', function () {
    global $post;

    if (! is_object($post)) {
        return;
    }

    $post = get_post($post->ID);

    if (is_single()) {
        $title = get_the_title();
    } else {
        $title = get_bloginfo('name');
    }

    if (is_single()) {
        $excerpt = get_the_excerpt($post->ID);
    } else {
        $excerpt = get_bloginfo('description');
    }

    if (! is_single() || ! has_post_thumbnail($post->ID)) {
        $image = get_stylesheet_directory_uri() . '/assets/images/opengraph_image.jpg';
    } else {
        $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'opengraph_image', false);
        $image = get_bloginfo('url') . $image[0];
    }

    if (is_single()) {
        $url  = get_the_permalink($post->ID);
        $type = 'article';
    } else {
        $url  = get_bloginfo('url');
        $type = 'website';
    }
    ?>

    <meta name="description" content="<?= $excerpt; ?>">

    <!-- Facebook Metatags -->
    <meta property="fb:admins" content="1111246058"/>
    <meta property="fb:app_id" content="410360185726979"/>

    <!-- OpenGraph Metatags -->
    <meta property="og:site_name" content="<?= get_bloginfo('name'); ?>">
    <meta property="og:title" content="<?= $title; ?>">
    <meta property="og:description" content="<?= $excerpt; ?>">
    <meta property="og:image" content="<?= $image; ?>">
    <meta property="og:url" content="<?= $url; ?>">
    <meta property="og:type" content="<?= $type; ?>">

    <!-- Twitter Card Metatags -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@theYahzee">
    <meta name="twitter:domain" content="theYahzee">
    <meta name="twitter:creator" content="@theYahzee">
    <meta name="twitter:title" content="<?= $title; ?>">
    <meta name="twitter:description" content="<?= $excerpt; ?>">
    <meta name="twitter:image:src" content="<?= $image; ?>">

    <?php
}, 2);

/**
 * Disable Jetpack metatags
 */
add_filter('jetpack_enable_open_graph', '__return_false');

/**
 * Add language attributes
 */
add_filter('language_attributes', function ($output) {
    return $output . ' xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml"';
}, 10, 2);

/**
 * Build JSON-LD
 */
function spiral_schema()
{
    global $post;

    if (is_single()) {
        if ('post' == get_post_type()) {
            $type = 'NewsArticle';
        } elseif (get_post_type() == 'editorial') {
            $type = 'Article';
        } elseif (get_post_type() == 'review') {
            $type = 'Review';
        } elseif (get_post_type() == 'soundtrack') {
            $type = 'Album';
        }
    } else {
        $type = 'WebSite';
    }

    $published = get_the_date();
    $updated   = get_the_modified_date();
    $title     = get_the_title();
    $site      = get_bloginfo('name');

    if (! is_single() || ! has_post_thumbnail($post->ID)) {
        $image = get_stylesheet_directory_uri() . '/assets/images/opengraph_image.jpg';
    } else {
        $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'opengraph_image', false);
        $image = get_bloginfo('url') . $image[0];
    }

    $movie = get_the_terms(get_the_ID(), 'movies');

    $output = '<script type="application/ld+json">{';

    if (is_single()) {
        $output .= '"@context" : "http://schema.org",';
        $output .= '"@type"    : "' . $type . '",';
        $output .= '"author"   : { {';
        $output .= '"@type" : "Person",';
        $output .= '"name"  : "Yahzee Skellington",';
        $output .= '"sameAs": "http://senzafine.net"';
        $output .= '},';
        $output .= '"datePublished": "' . $published . '",';
        $output .= '"dateModified" : "' . $updated . '",';
        $output .= '"headline"     : "' . $title .'",';
        $output .= '"description"  : "' . get_the_excerpt() .'",';
        $output .= '"image": {';
        $output .= '"@type" : "ImageObject",';
        $output .= '"url"   : "' . $image .'",';
        $output .= '"width" : "600",';
        $output .= '"height": "315"';
        $output .= '},';

        if ('review' == get_post_type()) {
            $output .= '"itemReviewed": {';
            $output .= '"@type" : "Movie",';
            $output .= '"image" : "' . $image .'",';
            $output .= '"name"  : "' . $movie[0]->name .'",';
            $output .= '"sameAs": "' . get_the_permalink() .'"';
            $output .= '},';
        }

        $output .= '"publisher": {';
        $output .= '"@type": "Organization",';
        $output .= '"name" : "' . $site . '",';
        $output .= '"logo": {';
        $output .= '"@type": "ImageObject",';
        $output .= '"url"  : "' . get_stylesheet_directory_uri() . '/assets/images/logo.png' . '"';
        $output .= '}';
        $output .= '},';
        $output .= '"mainEntityOfPage": {';
        $output .= '"@type": "WebPage",';
        $output .= '"@id"  : "' . get_bloginfo('url') . '"';
        $output .= '}';
    } else {
        $output .= '"@context" : "http://schema.org",';
        $output .= '"@type"    : "' . $type . '",';
        $output .= '"@id"      : "' . get_bloginfo('url') . '"';
    }

    $output .= '}</script>';

    echo $output;
}
