<?php

/**
 * Change url of the login logo
 */
add_filter('login_headerurl', function () {
    return get_bloginfo('wpurl');
});

/**
 * Change the title of the login logo
 */
add_filter('login_headertitle', function () {
    return __('Made by Senzafine and powered by WordPress', 'spiral');
});
