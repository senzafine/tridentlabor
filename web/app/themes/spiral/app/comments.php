<?php

/**
 * Comment form template
 */
function spiral_comment_form()
{
    $commenter     = wp_get_current_commenter();
    $user          = wp_get_current_user();
    $user_identity = $user->exists() ? $user->display_name : '';

    $req           = get_option('require_name_email');
    $aria_req      = ($req ? " aria-required='true'" : '');
    $html_req      = ($req ? " required='required'" : '');
    $html5         = 'html5' === $args['format'];
    $required_text = __('Required fields are highlighted.', 'spiral');

    $args = [
        'fields' => apply_filters(
            'comment_form_default_fields',
            [
                'author' => '
                    <label for="author" class="comment-input">
                    <input type="text" name="author" placeholder="' . __('Name', 'spiral') . '" value="' . esc_attr($commenter['comment_author']) . '" ' . $aria_req . ' id="author" ' . ($req ? 'class="required"' : '') . '>
                    </label>',
                'email' => '
                    <label for="email" class="comment-input">
                    <input type="text" name="email" placeholder="' . __('Email', 'spiral') . '" value="' . esc_attr($commenter['comment_author_email']) . '" ' . $aria_req . ' id="email" ' . ($req ? 'class="required"' : '') . '>
                    </label>',
                'url' => '
                    <label for="url" class="comment-input">
                    <input type="text" name="url" placeholder="' . __('Website', 'spiral') . '" value="' . esc_attr($commenter['comment_author_url']) . '" id="url">
                    </label>'
            ]
        ),

        'comment_field' => '
            <label for="comment" class="comment-input">
            <textarea name="comment" placeholder="' . __('Write your comment...', 'spiral') . '" aria-required="true" id="comment"></textarea>
            </label>',

        'must_log_in' => '<div class="alert alert-warning">' . sprintf(__('You must be <a href="%s">logged in</a> to post a comment.', 'spiral'), wp_login_url(apply_filters('the_permalink', get_permalink()))) . '.</div>',

        'logged_in_as' => '<p>' . sprintf(__('Logged in as <a href="%1$s" aria-label="%2$s">%3$s</a>. <a href="%4$s">Log out?</a>', 'spiral'), admin_url('profile.php'), esc_attr(sprintf(__('Logged in as %s. Edit your profile.', 'spiral'), $user_identity)), $user_identity, wp_logout_url(apply_filters('the_permalink', get_permalink($post_id)))) . '</p>',

        'comment_notes_before' => '<p class="small">' . __('Your email address will not be published.', 'spiral') . ' ' . ($req ? $required_text : '') . '</p>',

        'comment_notes_after' => '',
        'id_form'             => 'comment-form',
        'class_form'          => 'comment-form',
        'id_submit'           => 'comment-submit',
        'class_submit'        => 'comment-submit',
        'title_reply'         => __('What do you think?', 'spiral'),
        'title_reply_to'      => __('Reply to %s', 'spiral'),
        'title_reply_before'  => '<h2>',
        'title_reply_after'   => '</h2>',
        'cancel_reply_before' => ' <small>',
        'cancel_reply_after'  => '</small>',
        'cancel_reply_link'   => __('Cancel Comment', 'spiral'),
        'label_submit'        => __('Post Comment', 'spiral')
    ];

    return $args;
}

/**
 * Display single comment
 */
function spiral_comment($comment, $args, $depth)
{
    $GLOBALS['comment'] = $comment;
    ?>

    <li <?php comment_class('comment'); ?> id="comment-<?php comment_ID(); ?>">
        <figure class="comment-avatar">
            <a href="<?php comment_author_url(); ?>" alt="<?php comment_author(); ?>"><?= get_avatar($comment); ?></a>
        </figure>
        <div class="comment-body">
            <h3><a href="<?php comment_author_url(); ?>"><?php comment_author(); ?></a></h3>
            <div class="meta">
                <time datetime="<?= get_comment_time('c'); ?>" class="meta-time"><?= sprintf(__('%1$s at %2$s', 'spiral'), get_comment_date(), get_comment_time()); ?></time>
            </div>

            <?php if (empty($comment->comment_approved)) : ?>
                <div class="alert alert-warning"><?= __('Your comment is awaiting moderation.', 'spiral'); ?></div>
            <?php endif; ?>

            <?php comment_text(); ?>

            <?php if (comments_open()) : ?>
                <p class="comment-reply">
                    <?php comment_reply_link(array_merge($args, [
                        'reply_text' => __('Reply', 'spiral'),
                        'depth'      => $depth,
                        'max_depth'  => $args['max_depth']
                    ])); ?>
                </p>
            <?php endif; ?>
        </div>
    </li>
    <?php
}

/**
 * Display pingbacks & trackbacks
 */
function spiral_pingback($comment, $args, $depth)
{
    $GLOBALS['comment'] = $comment;
    ?>

    <li <?php comment_class('ping'); ?> id="comment-<?php comment_ID(); ?>">
        <p><a href="<?php comment_author_url(); ?>"><?php comment_author(); ?></a></p>

        <?php if (empty($comment->comment_approved)) : ?>
            <div class="alert alert-warning"><?= __('Your comment is awaiting moderation.', 'spiral'); ?></div>
        <?php endif; ?>
    </li>
    <?php
}

/**
 * Get the number of comments
 */
function spiral_comments_number()
{
    global $id;

    $comments = get_approved_comments($id);
    $count    = 0;

    foreach ($comments as $comment) {
        if (empty($comment->comment_type)) {
            $count++;
        }
    }

    return $count;
}

/**
 * Get the textual number of comments
 */
function spiral_comments_number_text()
{
    $comments_number = spiral_comments_number();

    if (0 === $comments_number) {
        $output = __('No comments so far', 'spiral');
    } elseif (1 === $comments_number) {
        $output = __('One comment so far', 'spiral');
    } else {
        $output = sprintf(__('%s comments so far', 'spiral'));
    }

    echo $output;
}

/**
 * Display comments in the sidebar
 */
function spiral_sidebar_comments($no_comments = 5, $comment_len = 150, $avatar_size = 48)
{
    $query    = new WP_Comment_Query();
    $comments = $query->query(['number' => $no_comments]);
    $output   = '';

    if ($comments) {
        $output .= '<ul>';

        foreach ($comments as $comment) {
            $output .= '<li class="comment">';
            $output .= '<figure class="comment-avatar">';
            $output .= '<a href="' . get_permalink($comment->comment_post_ID) . '">';
            $output .= get_avatar($comment->comment_author_email, $avatar_size);
            $output .= '</a>';
            $output .= '</figure>';
            $output .= '<div class="comment-body">';
            $output .= '<a href="' . get_permalink($comment->comment_post_ID) . '">';
            $output .= '<p>' . strip_tags(substr(apply_filters('get_comment_text', $comment->comment_content), 0, $comment_len)) . '</p>';
            $output .= '</a>';
            $output .= '<cite>' . get_comment_author($comment->comment_ID) . '</cite>';
            $output .= '</div>';
            $output .= '</li>';
        }

        $output .= '</ul>';
    } else {
        $output .= '<p>' . __('No comments.', 'spiral') . '</p>';
    }

    echo $output;
}

/**
 * Prevent Disqus from replacing comment count
 */
remove_filter('comments_number', 'dsq_comments_text');
remove_filter('get_comments_number', 'dsq_comments_number');
remove_action('loop_end', 'dsq_loop_end');
