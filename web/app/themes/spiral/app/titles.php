<?php

namespace App;

/**
 * Page titles
 * @return string
 */
function title()
{
    if (is_home()) {
        if ($home = get_option('page_for_posts', true)) {
            return get_the_title($home);
        }

        return __('Latest Posts', 'spiral');
    }

    if (is_archive()) {
        if (is_category()) {
            if (!is_category('news') && !is_category('features')) {
                return sprintf('%1$s <span class="post-type">%2$s</span>', single_cat_title('', false), __('News', 'spiral'));
            } else {
                return sprintf(single_cat_title('', false));
            }
        } elseif (is_tag()) {
            return sprintf('%1$s <span class="post-type">%2$s</span>', __('Tag', 'spiral'), single_tag_title('', false));
        } elseif (is_author()) {
            return sprintf('%1$s <span class="post-type">%2$s</span>', __('Author', 'spiral'), get_the_author());
        } elseif (is_year()) {
            return sprintf('%1$s <span class="post-type">%2$s</span>', __('Year', 'spiral'), get_the_date('Y', 'yearly archives date format'));
        } elseif (is_month()) {
            return sprintf('%1$s <span class="post-type">%2$s</span>', __('Month', 'spiral'), get_the_date('F Y', 'monthly archives date format'));
        } elseif (is_day()) {
            return sprintf('%1$s <span class="post-type">%2$s</span>', __('Day', 'spiral'), get_the_date('F j, Y', 'daily archives date format'));
        } elseif (is_tax('post_format')) {
            if (is_tax('post_format', 'post-format-aside')) {
                return _x('Asides', 'post format archive title');
            } elseif (is_tax('post_format', 'post-format-gallery')) {
                return _x('Galleries', 'post format archive title');
            } elseif (is_tax('post_format', 'post-format-image')) {
                return _x('Images', 'post format archive title');
            } elseif (is_tax('post_format', 'post-format-video')) {
                return _x('Videos', 'post format archive title');
            } elseif (is_tax('post_format', 'post-format-quote')) {
                return _x('Quotes', 'post format archive title');
            } elseif (is_tax('post_format', 'post-format-link')) {
                return _x('Links', 'post format archive title');
            } elseif (is_tax('post_format', 'post-format-status')) {
                return _x('Statuses', 'post format archive title');
            } elseif (is_tax('post_format', 'post-format-audio')) {
                return _x('Audio', 'post format archive title');
            } elseif (is_tax('post_format', 'post-format-chat')) {
                return _x('Chats', 'post format archive title');
            }
        } elseif (is_post_type_archive()) {
            return post_type_archive_title('', false);
        } elseif (is_tax()) {
            $taxonomy = get_taxonomy(get_queried_object()->$taxonomy);
            return sprintf('%1$s <span class="post-type">%2$s</span>', $tax->labels->singular_name, single_term_title('', false));
        } else {
            return __('Archive', 'spiral');
        }
    }

    if (is_singular()) {
        if ('gallery' == get_post_type()) {
            return sprintf('%1$s <span class="post-type">%2$s</span>', get_the_title(), __('Gallery', 'spiral'));
        }

        if ('review' == get_post_type() && !is_category('revisited')) {
            return sprintf('%1$s <span class="post-type">%2$s</span>', get_the_title(), __('Review', 'spiral'));
        }
    }

    if (is_search()) {
        return sprintf('%1$s <span class="search-term">%2$s</span>', __('Search', 'spiral'), get_search_query());
    }

    if (is_404()) {
        return __('Not Found', 'spiral');
    }

    return get_the_title();
}

/**
 * Hook into the page title metatag
 */
add_filter('wp_title', function ($title, $sep) {
    $site = get_bloginfo('name');
    $title = strip_tags(title());

    if (is_home()) {
        return $site;
    } else {
        return $title . ' ' . $sep . ' ' . $site;
    }
}, 10, 2);
