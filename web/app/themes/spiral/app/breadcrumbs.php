<?php

/**
 * Breadcrumbs
 */
function spiral_breadcrumbs()
{
    $breadcrums_id    = 'breadcrumbs';
    $breadcrums_class = 'breadcrumbs';
    $home_title       = __('Home', 'spiral');
    $custom_taxonomy  = '';

    global $post, $wp_query;

    if (!is_front_page()) {
        $output = '<div class="breadcrumbs">';
        $output .= '<div class="wrapper">';
        $output .= '<ul>';
        $output .= '<li class="item-home"><a href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';

        if (is_archive() && !is_tax() && !is_category() && !is_tag()) {
            $output .= '<li class="item-current">' . post_type_archive_title('', false) . '</li>';
        } elseif (is_archive() && is_tax() && !is_category() && !is_tag()) {
            $post_type = get_post_type();

            if ('post' !== $post_type) {
                $post_type_object  = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);

                $output .= '<li><a href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
            }

            $custom_tax_name = get_queried_object()->name;

            $output .= '<li class="item-current">' . $custom_tax_name . '</li>';
        } elseif (is_single()) {
            $post_type = get_post_type();

            if ('post' !== $post_type) {
                $post_type_object  = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);

                $output .= '<li><a href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
            }

            $category = get_the_category();

            if (!empty($category)) {
                $category             = array_values($category);
                $last_category        = end($category);
                $get_category_parents = rtrim(get_category_parents($last_category->term_id, true, ','), ',');
                $category_parents     = explode(',', $get_category_parents);
                $category_display     = '';

                foreach ($category_parents as $parents) {
                    $category_display .= '<li>' . $parents . '</li>';
                }
            }

            $taxonomy_exists = taxonomy_exists($custom_taxonomy);

            if (empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                $taxonomy_terms = get_the_terms($post->ID, $custom_taxonomy);
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
            }

            if (!empty($last_category)) {
                $output .= $category_display;
                $output .= '<li class="item-current">' . get_the_title() . '</li>';
            } elseif (!empty($cat_id)) {
                $output .= '<li><a href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
                $output .= '<li class="item-current">' . get_the_title() . '</li>';
            } else {
                $output .= '<li class="item-current">' . get_the_title() . '</li>';
            }
        } elseif (is_category()) {
            $output .= '<li class="item-current">' . single_cat_title('', false) . '</li>';
        } elseif (is_page()) {
            if ($post->post_parent) {
                $ancestors = get_post_ancestors($post->ID);
                $ancestors = array_reverse($ancestors);

                if (!isset($parents)) {
                    $parents = null;
                }

                foreach ($ancestors as $ancestor) {
                    $parents .= '<li><a href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                }

                $output .= $parents;
                $output .= '<li class="item-current">' . get_the_title() . '</li>';
            } else {
                $output .= '<li class="item-current">' . get_the_title() . '</li>';
            }
        } elseif (is_tag()) {
            $term_id       = get_query_var('tag_id');
            $taxonomy      = 'post_tag';
            $args          = 'include=' . $term_id;
            $terms         = get_terms($taxonomy, $args);
            $get_term_id   = $terms[0]->term_id;
            $get_term_slug = $terms[0]->slug;
            $get_term_name = $terms[0]->name;

            $output .= '<li class="item-current">' . $get_term_name . '</li>';
        } elseif (is_day()) {
            $output .= '<li><a href="' . get_year_link(get_the_time('Y')) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archive</a></li>';
            $output .= '<li><a href="' . get_month_link(get_the_time('Y'), get_the_time('m')) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archive</a></li>';
            $output .= '<li class="item-current">' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archive</li>';
        } elseif (is_month()) {
            $output .= '<li><a href="' . get_year_link(get_the_time('Y')) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archive</a></li>';
            $output .= '<li>' . get_the_time('M') . ' Archives</li>';
        } elseif (is_year()) {
            $output .= '<li class="item-current">' . get_the_time('Y') . ' Archives</li>';
        } elseif (is_author()) {
            global $author;

            $userdata = get_userdata($author);

            $output .= '<li class="item-current">' . __('Author', 'spiral') . ': ' . $userdata->display_name . '</li>';
        } elseif (get_query_var('paged')) {
            $output .= '<li class="item-current">' . __('Page', 'spiral') . ': ' . get_query_var('paged') . '</li>';
        } elseif (is_search()) {
            $output .= '<li class="item-current">' . __('Search', 'spiral') . ': ' . get_search_query() . '</li>';
        } elseif (is_404()) {
            $output .= '<li class="item-current">' . __('404', 'spiral') . '</li>';
        }

        $output .= '</ul>';
        $output .= '</div>';
        $output .= '</div>';

        echo $output;
    }
}
