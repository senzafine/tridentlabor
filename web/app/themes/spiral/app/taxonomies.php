<?php

/**
 * Define custom taxonomies
 */
add_action('init', function () {
    $movies = [
        'labels' => [
            'name'          => __('Movies', 'spiral'),
            'singular_name' => __('Movie', 'spiral'),
            'menu_name'     => __('Movies', 'spiral'),
            'add_new_item'  => __('Add New Movie', 'spiral'),
            'new_item_name' => __('New Movie', 'spiral'),
            'edit_item'     => __('Edit Movie', 'spiral'),
            'all_items'     => __('All Movies', 'spiral'),
            'search_items'  => __('Search Movies', 'spiral'),
        ],
        'rewrite' => [
            'slug' => 'movie'
        ],
        'hierarchical'      => false,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true
    ];

    $franchises = [
        'labels' => [
            'name'          => __('Franchises', 'spiral'),
            'singular_name' => __('Franchise', 'spiral'),
            'menu_name'     => __('Franchises', 'spiral'),
            'add_new_item'  => __('Add New Franchise', 'spiral'),
            'new_item_name' => __('New Franchise', 'spiral'),
            'edit_item'     => __('Edit Franchise', 'spiral'),
            'all_items'     => __('All Franchises', 'spiral'),
            'search_items'  => __('Search Franchises', 'spiral'),
        ],
        'rewrite' => [
            'slug' => 'franchise'
        ],
        'hierarchical'      => false,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true
    ];

    register_taxonomy('movies', ['post','movie','soundtrack','review','editorial','gallery'], $movies);
    register_taxonomy('franchise', 'movie', $franchises);
    register_taxonomy_for_object_type('tag', 'movie');
    register_taxonomy_for_object_type('tag', 'soundtrack');
    register_taxonomy_for_object_type('tag', 'review');
    register_taxonomy_for_object_type('tag', 'gallery');
    register_taxonomy_for_object_type('tag', 'editorial');
});
