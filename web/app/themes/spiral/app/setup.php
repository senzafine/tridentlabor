<?php

namespace App;

use Illuminate\Contracts\Container\Container as ContainerContract;
use Senzafine\Spiral\Assets\JsonManifest;
use Senzafine\Spiral\Config;
use Senzafine\Spiral\Template\Blade;
use Senzafine\Spiral\Template\BladeProvider;

/**
 * Frontend assets
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('spiral/fonts', google_fonts(), false);
    wp_enqueue_style('spiral/vendor.css', asset_path('styles/vendor.css'), false, null);
    wp_enqueue_style('spiral/main.css', asset_path('styles/main.css'), false, null);
    wp_enqueue_script('spiral/manifest.js', asset_path('scripts/manifest.js'), false, null, true);
    wp_enqueue_script('spiral/vendor.js', asset_path('scripts/vendor.js'), false, null, true);
    wp_enqueue_script('spiral/main.js', asset_path('scripts/main.js'), false, null, true);
}, 100);

/**
 * Dashboard assets
 */
add_action('admin_enqueue_scripts', function () {
    wp_enqueue_style('spiral/fonts', google_fonts(), false);
    wp_enqueue_style('spiral/vendor.css', asset_path('styles/vendor.css'), false, null);
    wp_enqueue_style('spiral/admin.css', asset_path('styles/admin.css'), false, null);
    wp_enqueue_script('spiral/manifest.js', asset_path('scripts/manifest.js'), false, null, true);
    wp_enqueue_script('spiral/vendor.js', asset_path('scripts/vendor.js'), false, null, true);
    wp_enqueue_script('suggest');
    wp_enqueue_script('spiral/admin.js', asset_path('scripts/admin.js'), false, null, true);
}, 100);

/**
 * Login assets
 */
add_action('login_enqueue_scripts', function () {
    wp_enqueue_style('spiral/fonts', google_fonts(), false);
    wp_enqueue_style('spiral/vendor.css', asset_path('styles/vendor.css'), false, null);
    wp_enqueue_style('spiral/login.css', asset_path('styles/login.css'), false, null);
    wp_enqueue_script('spiral/manifest.js', asset_path('scripts/manifest.js'), false, null, true);
    wp_enqueue_script('spiral/vendor.js', asset_path('scripts/vendor.js'), false, null, true);
    wp_enqueue_script('spiral/login.js', asset_path('scripts/login.js'), false, null, true);
}, 100);

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support('soil-clean-up');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-relative-urls');

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
     register_nav_menus(
         [
             'main_menu'   => __('Main Menu', 'spiral'),
             'footer_menu' => __('Footer Menu', 'spiral')
         ]
     );

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    /**
     * Set defaults options for the theme
     */
    update_option('start_of_week', 0);
    update_option('thumbnail_size_w', 0);
    update_option('thumbnail_size_h', 0);
    update_option('medium_size_w', 0);
    update_option('medium_size_h', 0);
    update_option('medium_large_size_w', 0);
    update_option('medium_large_size_h', 0);
    update_option('large_size_w', 0);
    update_option('large_size_h', 0);

    /**
     * Enable post formats
     * @link http://codex.wordpress.org/Post_Formats
     */
    add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Make theme available for translation
     */
    load_theme_textdomain('spiral', get_template_directory() . '/languages');

    /**
     * Enable automatic feed links
     */
    add_theme_support('automatic-feed-links');

    /**
     * Disable admin bar on the frontend
     */
    show_admin_bar(false);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Use main stylesheet for visual editor
     * @see resources/assets/styles/layouts/_tinymce.scss
     */
    add_editor_style(asset_path('styles/editor.css'));
}, 20);

/**
 * Register sidebars
 */
add_action('widgets_init', function () {
    $config = [
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ];

    register_sidebar([
        'name'          => __('Primary', 'spiral'),
        'id'            => 'sidebar-primary'
    ] + $config);

    register_sidebar([
        'name'          => __('Footer', 'spiral'),
        'id'            => 'sidebar-footer'
    ] + $config);
});

/**
 * Define Google fonts
 */
function google_fonts()
{
    $fonts = '//fonts.googleapis.com/css?family=Droid+Serif:400,400i,700,700i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i';
    return $fonts;
}

/**
 * Custom buttons for the visual editor
 */
add_filter('mce_buttons_2', function ($buttons, $editor_id) {
    array_unshift($buttons, 'styleselect');
    return $buttons;
}, 10, 2);

add_filter('tiny_mce_before_init', function ($settings) {
    $style_formats = [
        [
            'title'   => 'Movie',
            'inline'  => 'cite',
            'classes' => 'movie'
        ],

        [
            'title'  => 'Quote',
            'inline' => 'q'
        ],

        [
            'title'  => 'Responsive Video',
            'inline' => 'div',
            'class'  => 'embed-video'
        ]
    ];

    $settings['style_formats'] = json_encode($style_formats);
    return $settings;
});

/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    spiral('blade')->share('post', $post);
});

/**
 * Setup Spiral options
 */
add_action('after_setup_theme', function () {
    /**
     * Spiral config
     */
    $paths = [
        'dir.stylesheet' => get_stylesheet_directory(),
        'dir.template'   => get_template_directory(),
        'dir.upload'     => wp_upload_dir()['basedir'],
        'uri.stylesheet' => get_stylesheet_directory_uri(),
        'uri.template'   => get_template_directory_uri(),
    ];

    $viewPaths = collect(preg_replace('%[\/]?(resources/views)?[\/.]*?$%', '', [STYLESHEETPATH, TEMPLATEPATH]))
        ->flatMap(function ($path) {
            return ["{$path}/resources/views", $path];
        })->unique()->toArray();

        // die(var_dump($viewPaths));
    config([
        'assets.manifest' => "{$paths['dir.stylesheet']}/../assets/assets.json",
        'assets.uri'      => "{$paths['uri.stylesheet']}/assets",
        'view.compiled'   => "{$paths['dir.upload']}/cache/compiled",
        'view.namespaces' => ['App' => WP_CONTENT_DIR],
        'view.paths'      => $viewPaths,
    ] + $paths);

    /**
     * Add JsonManifest to Spiral container
     */
    spiral()->singleton('spiral.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Spiral container
     */
    spiral()->singleton('spiral.blade', function (ContainerContract $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view'], $app);
    });

    /**
     * Create @asset() Blade directive
     */
    spiral('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= App\\asset_path({$asset}); ?>";
    });
});

/**
 * Init config
 */
spiral()->bindIf('config', Config::class, true);
