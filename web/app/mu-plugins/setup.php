<?php

/*
Plugin Name:  Basic Site Setup
Plugin URI:   http://senzafine.net/coil/
Description:  Sets up default options for the WordPress installation
Version:      1.0.1
Author:       Yahzee Skellington
Author URI:   http://senzafine.net/
*/

if (! is_blog_installed()) {
    return;
}

$document_root_remote = $_SERVER['DOCUMENT_ROOT'];
$document_root_local  = str_replace('/', '\\', $document_root_remote);

if (substr_compare(getcwd(), ":", 1, 1) == 0) {
    $uploads_folder = $document_root_local . '\media';
} else {
    $uploads_folder = $document_root_remote . '/current/web/media';
}

//if ('http://' . $_SERVER['SERVER_NAME'] . '/wp' === get_option('home')) {
    update_option('siteurl', 'http://' . $_SERVER['SERVER_NAME'] . '/wp');
    update_option('home', 'http://' . $_SERVER['SERVER_NAME']);
    update_option('uploads_use_yearmonth_folders', 0);
    update_option('permalink_structure', '/%category%/%postname%/');
    update_option('upload_path', $uploads_folder);
    update_option('upload_url_path', 'http://' . $_SERVER['SERVER_NAME'] . '/media');
//}
