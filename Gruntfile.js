module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    server: grunt.file.readJSON('server.json'),
    timestamp: grunt.template.today('mm-dd-yyyy_HH-MM-ss'),

    sshexec: {
      dump_remote_db: {
        options: {
          host: '<%= server.remote.host %>',
          port: '<%= server.remote.port %>',
          username: '<%= server.remote.username %>',
          agent: process.env.SSH_AUTH_SOCK
        },
        command: [
          'cd <%= server.remote.save_path %>',
          'mysqldump -u <%= server.remote.dbuser %> -p<%= server.remote.dbpass %> <%= server.remote.dbname %> | gzip > remote_<%= server.remote.filename %>.sql.gz'
        ].join(' && ')
      },
      cleanup_remote_db: {
        options: {
          host: '<%= server.remote.host %>',
          port: '<%= server.remote.port %>',
          username: '<%= server.remote.username %>',
          agent: process.env.SSH_AUTH_SOCK
        },
        command: [
          'cd <%= server.remote.save_path %>',
          'rm remote_<%= server.remote.filename %>.sql.gz'
        ].join(' && ')
      },
      import_local_db: {
        options: {
          host: '<%= server.remote.host %>',
          port: '<%= server.remote.port %>',
          username: '<%= server.remote.username %>',
          agent: process.env.SSH_AUTH_SOCK
        },
        command: [
          'cd <%= server.remote.save_path %>',
          'gunzip < local_<%= server.local.filename %>.sql.gz | mysql -u <%= server.remote.dbuser %> -p<%= server.remote.dbpass %> <%= server.remote.dbname %>'
        ].join(' && ')
      },
      cleanup_local_db: {
        options: {
          host: '<%= server.remote.host %>',
          port: '<%= server.remote.port %>',
          username: '<%= server.remote.username %>',
          agent: process.env.SSH_AUTH_SOCK
        },
        command: [
          'cd <%= server.remote.save_path %>',
          'rm local_<%= server.local.filename %>.sql.gz'
        ].join(' && ')
      }
    },

    exec: {
      dump_local_db: {
        command: 'mysqldump -u <%= server.local.dbuser %> -p<%= server.local.dbpass %> <%= server.local.dbname %> > local_<%= server.local.filename %>.sql'
      },
      compress_local_db: {
        command: 'gzip local_<%= server.local.filename %>.sql'
      },
      upload_local_db: {
        command: 'scp local_<%= server.local.filename %>.sql.gz -p7822 <%= server.remote.username %>@<%= server.remote.host %>:<%= server.remote.save_path %>'
      },
      cleanup_local_db: {
        command: 'rm -rf local_<%= server.local.filename %>.sql'
      },
      get_remote_db: {
        command: 'wget -nv <%= server.remote.save_url %>/remote_<%= server.remote.filename %>.sql.gz'
      },
      decompress_remote_db: {
        command: 'gzip -d remote_<%= server.remote.filename %>.sql.gz'
      },
      import_remote_db: {
        command: 'mysql -u <%= server.local.dbuser %> -p"<%= server.local.dbpass %>" <%= server.local.dbname %> < remote_<%= server.remote.filename %>.sql'
      },
      cleanup_remote_db: {
        command: 'rm -rf remote_<%= server.remote.filename %>.sql'
      }
    },

    peach: {
      update_local_db: {
        options: {
          force: true
        },
        src: 'local_<%= server.local.filename %>.sql',
        dest: 'local_<%= server.remote.filename %>.sql',
        from: '<%= server.local.site_url %>',
        to: '<%= server.remote.site_url %>'
      },
      update_remote_db: {
        options: {
          force: true
        },
        src: 'remote_<%= server.remote.filename %>.sql',
        dest: 'remote_<%= server.local.filename %>.sql',
        from: '<%= server.remote.site_url %>',
        to: '<%= server.local.site_url %>'
      },
      update_content: {
        options: {
          force: true
        },
        src: '<%= server.remote.filename %>.sql',
        dest: '<%= server.remote.filename %>.sql',
        from: 'wp-content',
        to: 'content'
      }
    },

    rsync: {
      options: {
        args: ['--verbose --compress --times --progress'],
        exclude: ['.git*', '*.scss', 'node_modules'],
        recursive: true,
        ssh: true
      },
      sync_local_media: {
        options: {
          src: '<%= server.remote.username %>@<%= server.remote.host %>:~<%= server.remote.uploads_path %>/',
          dest: '<%= server.local.uploads_path %>',
          delete: true,
          ssh :true,
          port: 7822,
          recursive: true,
        }
      },
      sync_remote_media: {
        options: {
          src: '<%= server.local.uploads_path %>/',
          dest: '<%= server.remote.username %>@<%= server.remote.host %>:~<%= server.remote.uploads_path %>',
          delete: true,
          ssh :true,
          port: 7822,
          recursive: true,
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-exec');
  grunt.loadNpmTasks('grunt-peach');
  grunt.loadNpmTasks('grunt-rsync');
  grunt.loadNpmTasks('grunt-ssh');

  grunt.registerTask('sync_local_db', [
   'sshexec:dump_remote_db',
   'exec:get_remote_db',
   'sshexec:cleanup_remote_db',
   'exec:decompress_remote_db',
   'peach:update_remote_db',
   'exec:import_remote_db',
   'exec:cleanup_remote_db'
  ]);

  grunt.registerTask('sync_remote_db', [
    'exec:dump_local_db',
    'peach:update_local_db',
    'exec:compress_local_db',
    'exec:upload_local_db',
    'sshexec:import_local_db',
    'sshexec:cleanup_local_db',
    'exec:cleanup_local_db'
  ]);

  grunt.registerTask('sync_local_media', [
    'rsync:sync_local_media'
  ]);

  grunt.registerTask('sync_remote_media', [
    'rsync:sync_remote_media'
  ]);
};
