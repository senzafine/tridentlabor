# Define app name
set :application, 'trident'

# Define app repo address
set :repo_url, 'git@gitlab.com:senzafine/tridentlabor.git'

# Define repo branch
set :branch, :master

# Define deploy folder
set :deploy_to, (-> { "/home/senzafin/public_html/test/#{fetch(:application)}" })

# Define the debug level
set :log_level, :info

# Define the persistent files across deploys
# set :linked_files, fetch(:linked_files, []).push('.env')
set :linked_files, %w[.env web/.htaccess]

# Define the persistent directories across deploys
# set :linked_dirs, fetch(:linked_dirs, []).push('web/app/uploads')
set :linked_dirs, %w[web/media]

# Define a temp directory
set :tmp_dir, '/home/senzafin/capistrano_tmp'

# Define the path for Composer on the server
SSHKit.config.command_map[:composer] = '~/bin/composer.phar'

# Define the deploy tasks. Uncomment the 'after' calls to enable
namespace :deploy do
  desc 'Update WordPress template root paths to point to the new release'
  task :update_option_paths do
    on roles(:app) do
      within fetch(:release_path) do
        if test :wp, :core, 'is-installed'
          %i[stylesheet_root template_root].each do |option|
            # Only change the value if it's an absolute path
            # i.e. The relative path "/themes" must remain unchanged
            # Also, the option might not be set, in which case we leave it like that
            value = capture :wp, :option, :get, option, raise_on_non_zero_exit: false
            if value != '' && value != '/themes'
              execute :wp, :option, :set, option, fetch(:release_path).join('web/wp/wp-content/themes')
            end
          end
        end
      end
    end
  end
end

after 'deploy:publishing', 'deploy:update_option_paths'

namespace :deploy do
  desc 'Install theme dependencies'
  task :setup_theme do
    on roles(:app) do
      execute "~/bin/composer.phar install -d /home/senzafin/public_html/test/#{fetch(:application)}/current/web/app/themes/spiral"
    end
  end
end

after 'deploy', 'deploy:setup_theme'

# Create symlinks again because Composer removes our web/wp/wp-content/gallery symlink.
namespace :deploy do
  task :fix_symlinks do
    Rake::Task['deploy:symlink:linked_dirs'].reenable
    Rake::Task['deploy:symlink:linked_dirs'].invoke
  end
end

# after 'deploy:finished', 'deploy:fix_symlinks'
