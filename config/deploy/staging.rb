# Define stage name
set :stage, :staging

# Define server connection settings
server 'a2ss40.a2hosting.com', user: 'senzafin', roles: %w[web app db], port: 7822

# Define deploy folder
set :deploy_to, (-> { "/home/senzafin/public_html/test/#{fetch(:application)}" })

# Define the environment with the .env file
fetch(:default_env)[:wp_env] = :staging
