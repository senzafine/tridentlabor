# Define stage name
set :stage, :production

# Define server connection settings
server 'a2ss40.a2hosting.com', user: 'senzafin', roles: %w[web app db], port: 7822

# Define deploy folder
set :deploy_to, (-> { "/home/senzafin/public_html/test/#{fetch(:application)}" })

# Define how many releases to keep in the server
set :keep_releases, 2

# Define the environment with the .env file
fetch(:default_env)[:wp_env] = :production
